/******************************************************************************
 * Filename: jTest.cpp
 * Author: Jens Bodal
 * Date: May 17, 2015
 * Description: driver stub for testing jqueue and jstack implementations
 *****************************************************************************/

#include "jstack.hpp"
#include "jqueue.hpp"

#include <iostream>
#include <stdexcept>
#include <string>

int main() {
    jens::jqueue<double> myQueue;
    double aNumber;
    
    aNumber = 666;
    std::cout << "Adding " << aNumber << " to the queue \n";
    myQueue.add(aNumber);

    aNumber = 17;
    std::cout << "Adding " << aNumber << " to the queue \n";
    myQueue.add(aNumber);

    aNumber = 202;
    std::cout << "Adding " << aNumber << " to the queue \n";
    myQueue.add(aNumber);

    int mSize = myQueue.size();
    double firstNumber;
    for (int i = 0; i < mSize; i++) {
        firstNumber = myQueue.remove();
        std::cout << "Removing: " << firstNumber << '\n';
    }

    jens::jstack<std::string> myStack;
    std::string aString;


    try {
        aString = myStack.remove();
    }
    catch (const std::exception &e) {
        std::cout << e.what() << '\n';
    }

    aString = "Hello";
    std::cout << "Adding \"" << aString << "\" to the stack_list\n";
    myStack.add(aString);

    aString = "World";
    std::cout << "Adding \"" << aString << "\" to the stack_list\n";
    myStack.add(aString);
    
    aString = "This is a longer string?";
    std::cout << "Adding \"" << aString << "\" to the stack_list\n";
    myStack.add(aString);

    aString = "This is the last string added";
    std::cout << "Adding \"" << aString << "\" to the stack_list\n";
    myStack.add(aString);

    
    mSize = myStack.size();
    std::string lastString;
    for (int i = 0; i < mSize; i++) {
        lastString = myStack.remove();
        std::cout << "Removing: " << lastString << '\n';
    }
    

    return 0;
}
