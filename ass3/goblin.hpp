/******************************************************************************
 * Filename: goblin.hpp
 * Author: Jens Bodal
 * Date: May 07, 2015
 * Description: definition of a goblin creature 
 *****************************************************************************/

#ifndef GOBLIN_HPP
#define GOBLIN_HPP

#include "creature.hpp"

class Goblin : public Creature {
    public:
        Goblin() : Creature(Defaults()) { }
        struct Defaults : public Creature::Properties {
            Defaults();
        };
        virtual bool attack(Creature *creature);
    private:
        void achillesAttack(Creature *creature);
};

#endif
