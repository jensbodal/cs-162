/******************************************************************************
 * Filename: theShadow.hpp
 * Author: Jens Bodal
 * Date: May 10, 2015
 * Description: definition of The Shadow creature
 *****************************************************************************/

#ifndef THE_SHADOW_HPP
#define THE_SHADOW_HPP

#include "creature.hpp"

class TheShadow : public Creature {
    public:
        TheShadow() : Creature(Defaults()) { }
        struct Defaults : public Creature::Properties {
            Defaults();
        };
    protected:
        virtual void setStrength(int strength);
    private:
        static const double specialChance = 0.50;
};
#endif
