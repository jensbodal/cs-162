/******************************************************************************
 * Filename: blueMen.cpp
 * Author: Jens Bodal
 * Date: May 10, 2015
 * Description: implementation of the Blue Men class
 *****************************************************************************/

#include "blueMen.hpp"

BlueMen::Defaults::Defaults() {
    name = "Blue Men";
    setAttack(2, 10);
    setDefense(3, 6);
    armor = 3;
    strength = 12;
}

