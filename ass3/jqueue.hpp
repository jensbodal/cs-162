/******************************************************************************
 * Filename: jqueue.hpp
 * Author: Jens Bodal
 * Date: May 17, 2015
 * Description: a custom implementation of a queue-like data structure.  
 * The implementation is in the header file as otherwise we would need to
 * declare each type that the queue supports in the implementation file, such
 * as: template class jqueue<int>, template class jqueue<double>, etc...
 *****************************************************************************/

#ifndef JQUEUE_HPP
#define JQUEUE_HPP

#include <iostream>
#include <stdexcept>

namespace jens {

template <class T>  
class jqueue {
    private:
        int elements;

        struct Node {
            T value;
            Node *next;
            Node (T value1, Node *next1 = NULL) {
                value = value1;
                next = next1;
            }
        };
        Node *front;
        Node *rear;
        T lastRemoved;
    public:
        jqueue();
        ~jqueue();

        // Member functions //

        /***********************************************************************
         * Description: adds an object to the queue
         * Parameters: the object to add to the queue
         **********************************************************************/
        void add(T const& value);

        /***********************************************************************
         * Description: removes the first object from the queue and returns it 
         * as an object reference
         * Parameters: none
         **********************************************************************/
        T& remove();

        /***********************************************************************
         * Description: removes all of the objects from the queue
         * Parameters: none
         **********************************************************************/
        void clear();

        /***********************************************************************
         * Description: returns true if the queue is empty, false otherwise
         * Parameters: none
         **********************************************************************/
        bool isEmpty() { return this->elements == 0; }
        
        /***********************************************************************
         * Description: returns the number of objects that are in the queue
         * Parameters: none
         **********************************************************************/
        int size() { return this->elements; }

}; // end class jqueue declaration

template <class T>
jqueue<T>::jqueue() {
    front = NULL;
    rear = NULL;
    elements = 0;
}

template <class T>
jqueue<T>::~jqueue() {
    clear();
}

template <class T>
void jqueue<T>::add(T const& value) {
    if (isEmpty()) {
        front = new Node(value, NULL);
        rear = front;
    }
    else {
        rear->next = new Node(value, NULL);
        rear = rear->next;
    }
    ++elements;
}

template <class T>
T& jqueue<T>::remove() {
    Node *temp;
    if (isEmpty()) {
        throw std::runtime_error("Nothing to remove, queue is empty!");
    }
    else {
        temp = front;
        lastRemoved = front->value;
        front = front->next;
        delete temp;
        --elements;
        return lastRemoved;
    }
}

template <class T>
void jqueue<T>::clear() {
    while (!isEmpty()) {
        remove();
    }
}

} // end namespace jens

#endif
