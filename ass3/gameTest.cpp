/******************************************************************************
 * Filename: gameTest.cpp
 * Author: Jens Bodal
 * Date: May 07, 2015
 * Description: tests the implementation of the various Creatures in a
 * FantasyGame program
 * Input: none
 * Output: output of various test scenarios
 *****************************************************************************/

#include "creature.hpp"
#include "goblin.hpp"
#include "barbarian.hpp"
#include "reptilePeople.hpp"
#include "blueMen.hpp"
#include "theShadow.hpp"
#include "jqueue.hpp"
#include "jstack.hpp"

#include <iostream>

int main() {
    Goblin goblin;
    Barbarian barbarian;
    ReptilePeople reptile;
    BlueMen blueMen;
    TheShadow theShadow;
    Creature *creatures[] = {&goblin,&barbarian,&reptile,&blueMen,&theShadow};
    jens::jqueue<Creature*> qCreature;
    jens::jstack<Creature> jCreature;

    Creature *attacker;

    qCreature.add(&blueMen);
    qCreature.add(&goblin);
    qCreature.remove()->attack(qCreature.remove());
    std::cout << goblin.toString() << '\n';

    attacker = &goblin;
    std::cout << attacker->getName() << " is attacking...\n";
    for (int i = 0; i < 5; i++) {
        attacker->attack(creatures[i]);
        std::cout << creatures[i]->toString() << '\n';
    }
    std::cout << '\n';

    attacker = &barbarian;
    std::cout << attacker->getName() << " is attacking...\n";
    for (int i = 0; i < 5; i++) {
        attacker->attack(creatures[i]);
        std::cout << creatures[i]->toString() << '\n';
    }
    std::cout << '\n';
    
    attacker = &reptile;
    std::cout << attacker->getName() << " is attacking...\n";
    for (int i = 0; i < 5; i++) {
        attacker->attack(creatures[i]);
        std::cout << creatures[i]->toString() << '\n';
    }
    std::cout << '\n';
    
    attacker = &blueMen;
    std::cout << attacker->getName() << " is attacking...\n";
    for (int i = 0; i < 5; i++) {
        attacker->attack(creatures[i]);
        std::cout << creatures[i]->toString() << '\n';
    }
    std::cout << '\n';

    attacker = &theShadow;
    std::cout << attacker->getName() << " is attacking...\n";
    for (int i = 0; i < 5; i++) {
        attacker->attack(creatures[i]);
        std::cout << creatures[i]->toString() << '\n';
    }
    std::cout << "\n\n";
    std::cout << "Testing achilles attack on Blue Men\n\n";

    for (int i = 0; i < 100; i++) {
        Creature *testCreature = &blueMen;
        goblin.attack(testCreature);
        if (i % 10 == 0) {
            std::cout << testCreature->toString() << '\n';
        }
    }

}
