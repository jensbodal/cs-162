/******************************************************************************
 * Filename: reptilePeople.cpp
 * Author: Jens Bodal
 * Date: May 10, 2015
 * Description: implementation of the Reptile Men class
 *****************************************************************************/

#include "reptilePeople.hpp"

ReptilePeople::Defaults::Defaults() {
    name = "Reptile People";
    setAttack(3, 6);
    setDefense(1, 6);
    armor = 7;
    strength = 18;
}

