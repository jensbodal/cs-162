/******************************************************************************
 * Filename: jstack.hpp
 * Author: Jens Bodal
 * Date: May 17, 2015
 * Description: a custom implementation of a stack-like data structure.
 * The implementation is in the header file as otherwise we would need to
 * declare each type that the stack supports in the implementation file, such
 * as: template class jstack<int>, template class jstack<std::string>, etc...
 *****************************************************************************/

#ifndef JSTACK_HPP
#define JSTACK_HPP

#include <iostream>
#include <stdexcept>

namespace jens {

template <class T>

class jstack {
    private:
        int elements;

        struct Node {
            T value;
            Node *next;
            Node (T value1, Node *next1 = NULL) {
                value = value1;
                next = next1;
            }
        };
        Node *top;
        T lastRemoved;
    public:
        jstack() { top = NULL; elements = 0; }
        ~jstack() { clear(); }
       
        // Member functions //


        /***********************************************************************
         * Description: adds an object to the stack
         * Parameters: the object to add to the stack
         **********************************************************************/
        void add(T const& value);

        /***********************************************************************
         * Description: removes the last object from the stack and returns it as
         * a reference
         * Parameters: none
         **********************************************************************/
        T& remove();

        /***********************************************************************
         * Description: removes all of the objects from the stack
         * Parameters: none
         **********************************************************************/
        void clear();

        /***********************************************************************
         * Description: returns true if the stack is empty, false otherwise
         * Parameters: none
         **********************************************************************/
        bool isEmpty() { return this->elements == 0; }

        /***********************************************************************
         * Description: returns the number of objects that are in the stack
         * Parameters: none
         **********************************************************************/
        int size() { return this->elements; }

}; // end class jstack declaration

template <class T>
void jstack<T>::add(T const& value) {
    top = new Node(value, top);
    ++elements;
}

template <class T>
T& jstack<T>::remove() {
    Node *temp;
    
    if (isEmpty()) {
        throw std::runtime_error("Nothing to remove, stack is empty!");
    }

    else {
        temp = top;
        lastRemoved = top->value;
        top = top->next;
        delete temp;
        --elements;
        return lastRemoved;
    }
    
}

template <class T>
void jstack<T>::clear() { 
    while (!isEmpty()) {
        remove();
    }
}

} // end namespace jens

#endif
