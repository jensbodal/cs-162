/******************************************************************************
 * Filename: theShadow.cpp
 * Author: Jens Bodal
 * Date: May 10, 2015
 * Description: implementation of The Shadow class
 *****************************************************************************/

#include "theShadow.hpp"
#include <cstdlib>
#include <ctime>
#include <iostream>


TheShadow::Defaults::Defaults() {
    std::srand(std::time(NULL));
    name = "The Shadow";
    setAttack(2, 10);
    setDefense(1, 6);
    armor = 0;
    strength = 12;
}

void TheShadow::setStrength(int strength) {
    int chance = specialChance * 100; // normalize special chance to integer
    int specialRoll = std::rand() % 100 + 1;
    // if our special roll is greater than or equal to our special chance then
    // the ability triggers
    strength = chance < specialRoll ? this->getStrength() : strength;
    Creature::setStrength(strength);
}
