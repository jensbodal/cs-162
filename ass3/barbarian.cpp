/******************************************************************************
 * Filename: barbarian.cpp
 * Author: Jens Bodal
 * Date: May 08, 2015
 * Description: implementation of the barbarian class
 *****************************************************************************/

#include "barbarian.hpp"

Barbarian::Defaults::Defaults() {
    name = "Barbarian";
    setAttack(2, 6);
    setDefense(2, 6);
    armor = 0;
    strength = 12;
}

