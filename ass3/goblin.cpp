/******************************************************************************
 * Filename: goblin.cpp
 * Author: Jens Bodal
 * Date: May 07, 2015
 * Description: implementation of the Goblin class 
 *****************************************************************************/

#include "goblin.hpp"
#include <iostream>

Goblin::Defaults::Defaults() {
    name = "Goblin";
    setAttack(2, 6);
    setDefense(1, 6);
    armor = 3;
    strength = 8;
}

bool Goblin::attack(Creature *creature) {
    int totalAttack = this->getAttackRoll();
    if (totalAttack == 12) { this->achillesAttack(creature); }
    int totalDefense = Creature::getDefenseRoll(creature);
    return Creature::applyDamage(creature, totalAttack, totalDefense);
}

void Goblin::achillesAttack(Creature *creature) {
    double achillesMod = 0.50;
    if (creature->getName() != this->getName()) {
        Creature::setDefenseRollModifier(creature, achillesMod);
    }
}

