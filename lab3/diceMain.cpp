/******************************************************************************
 * Filename: DiceMain.cpp
 * Author: Jens Bodal
 * Date: April 16, 2015
 * Description: this is the main driver for the program with some helper
 * functions to ease writing information to a file. This program will quickly
 * allow you to set up various scenarios for rolling one or more dice, "loaded"
 * or regular, and write the output to file.
 * Input: none, hard coded input
 * Output: roll information to file if desired
 *****************************************************************************/

#include "dice.hpp"
#include "loadedDice.hpp"

#include <fstream> // for writing to file
#include <string> 
#include <sstream> // for building output string

std::ofstream outputFile;
std::string fileName = "2.pair.dice.1000.rolls.csv";

/***********************************************************************
 * Description: writes the roll type and its value to a file specified in
 * fileName field
 * Parameters: the roll type, the value of the roll
 **********************************************************************/
void writeRollToFile(std::string type, int value);

/***********************************************************************
 * Description: appends a string to a file, does not assume newline appended to
 * end
 * Parameters: the line to write to a file with newline included at end as
 * necessary
 **********************************************************************/
void writeToFile(std::string line);

int main() {
    int numberOfRolls = 1000;
    int regularDieSides = 10;
    int loadedDieSides = 10;
    std::string regular = "regular";
    std::string loaded = "loaded";

    Dice regularDie1(regularDieSides);
    Dice regularDie2(regularDieSides);
    LoadedDice loadedDie1(loadedDieSides);
    LoadedDice loadedDie2(loadedDieSides);
    
    outputFile.open(fileName.c_str());
    outputFile << "\"type\",\"value\"\n";
    outputFile.close();

    for (int i = 0; i < numberOfRolls; i++) {
        int roll1;
        int roll2;

        roll1 = regularDie1.roll();
        roll2 = regularDie2.roll();
        writeRollToFile(regular, roll1+roll2);

        roll1 = loadedDie1.roll();
        roll2 = loadedDie2.roll();
        writeRollToFile(loaded,roll1+roll2);
    }

    return 0;
}


void writeRollToFile(std::string type, int value) {
    std::stringstream output;
    char dquote = char(34);
    char delimiter = ',';
    output << dquote;
    output << type;
    output << dquote;
    output << delimiter;
    output << dquote;
    output << value;
    output << dquote;
    output << '\n';

    writeToFile(output.str());
}

void writeToFile(std::string line) {
    // appends text to file then closes it
    outputFile.open(fileName.c_str(), std::ofstream::app);
    outputFile << line;
    outputFile.close();
}

