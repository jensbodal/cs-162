/******************************************************************************
 * Filename: loadedDice.cpp
 * Author: Jens Bodal
 * Date: April 16, 2015
 * Description: LoadedDice is a child class of Dice.  A loaded dice returns the
 * value of the roll but doubled, if the value is greater than the specified
 * number of sides then it returns the maximum roll value possible
 *****************************************************************************/

#include "loadedDice.hpp"
#include <cstdlib>

LoadedDice::LoadedDice() : Dice() {}

LoadedDice::LoadedDice(int numSides) : Dice(numSides) {}

int LoadedDice::roll() {
    int result = Dice::roll() * 2;
    if (result > this->getNumberOfSides()) {
        result = this->getNumberOfSides();
    }
    return result;
}
