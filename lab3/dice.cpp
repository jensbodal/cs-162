/******************************************************************************
 * Filename: dice.cpp
 * Author: Jens Bodal
 * Date: April 16, 2015
 * Description: 
 * Input: 
 * Output: 
 *****************************************************************************/

#include "dice.hpp"
#include <cstdlib>
#include <ctime>

static const int DEFAULT_NUM_SIDES = 6;

Dice::Dice() {
    std::srand(time(NULL));
    setNumberOfSides(DEFAULT_NUM_SIDES);
}

Dice::Dice(int numSides) {
    std::srand(time(NULL));
    setNumberOfSides(numSides);
}

int Dice::roll() {
    int min = 1;
    int max = getNumberOfSides();
    return std::rand() % (max - min + 1) + min;
}
