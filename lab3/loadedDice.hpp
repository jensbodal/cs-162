/******************************************************************************
 * Filename: loadedDice.hpp
 * Author: Jens Bodal
 * Date: April 16, 2015
 * Description: defines a child class of Dice which is a "loaded" Dice object
 *****************************************************************************/

#ifndef LOADED_DICE_HPP
#define LOADED_DICE_HPP

#include "dice.hpp"

class LoadedDice : public Dice {
    public:
        /***********************************************************************
         * Description: default constructor for LoadedDice object, calls parent
         * constructor; sets number of sides to default value specified in
         * parent class
         * Parameters: none
         **********************************************************************/
        LoadedDice();
        
        /***********************************************************************
         * Description: createa a LoadedDice object with the specified number of
         * sides; calls the parent constructor
         * Parameters: 
         **********************************************************************/
        LoadedDice(int numSides);
        
        /***********************************************************************
         * Description: roll method for LoadedDice, takes the value of the roll
         * then doubles it, if the value is greater than the number of sides,
         * then sets the value to the number of sides
         * Parameters: none
         **********************************************************************/
        int roll();
};

#endif
