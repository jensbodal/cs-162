/******************************************************************************
 * Filename: dice.hpp
 * Author: Jens Bodal
 * Date: April 16, 2015
 * Description: defines a Dice object 
 *****************************************************************************/

#ifndef DICE_HPP
#define DICE_HPP

class Dice {
    private:
        int numSides;
        void setNumberOfSides(int numSides) { this->numSides = numSides; }
    public:
    
        /***********************************************************************
         * Description: Creates a dice object with a default number of sides
         * Parameters: none
         **********************************************************************/
        Dice();

        /***********************************************************************
         * Description: Creates a dice object with the specified number of sides
         * Parameters: integer value for number of sides for the die to have
         **********************************************************************/
        Dice(int numSides);

        int getNumberOfSides() { return this->numSides; }

        /***********************************************************************
         * Description: "Rolls" the dice; if a die has 6 sides then this
         * function will randomly return a value from 1 to 6
         * Parameters: none
         **********************************************************************/
        int roll();
};

#endif
