/******************************************************************************
 * Filename: upperFiveFilter.hpp
 * Author: Jens Bodal
 * Date: May 03, 2015
 * Description: 
 * Input: 
 * Output: 
 *****************************************************************************/

#ifndef UPPER_FIVE_FILTER_HPP
#define UPPER_FIVE_FILTER_HPP

#include "iFileFilter.hpp"
#include <fstream>

class UpperFiveFilter : virtual public IFileFilter {
    public:
        void doFilter(std::ifstream &in, std::ofstream &out);
        char transform(char ch);
};

#endif
