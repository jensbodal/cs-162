/******************************************************************************
 * Filename: originalFilter.hpp
 * Author: Jens Bodal
 * Date: May 03, 2015
 * Description: definition file for a file filter
 *****************************************************************************/

#ifndef ORIGINAL_FILTER_HPP
#define ORIGINAL_FILTER_HPP

#include "iFileFilter.hpp"
#include <fstream>

class OriginalFilter : virtual public IFileFilter {
    public:
        void doFilter(std::ifstream &in, std::ofstream &out);
        
        /***********************************************************************
         * Description: simply returns the character as we want to preserve the
         * original text
         * Parameters: the character to transform
         **********************************************************************/
        char transform(char ch);

};

#endif
