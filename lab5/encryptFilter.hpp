/******************************************************************************
 * Filename: encryptFilter.hpp
 * Author: Jens Bodal
 * Date: May 03, 2015
 * Description: 
 * Input: 
 * Output: 
 *****************************************************************************/

#ifndef ENCRYPT_FILTER_HPP
#define ENCRYPT_FILTER_HPP

#include "iFileFilter.hpp"

class EncryptFilter : virtual public IFileFilter {
    private:
        int encryptionKey;
    public:
        EncryptFilter();
        void promptForEncryptionKey();
        void doFilter(std::ifstream &in, std::ofstream &out);
        char transform(char ch);
};

#endif
