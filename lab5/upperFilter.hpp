/******************************************************************************
 * Filename: upperFilter.hpp
 * Author: Jens Bodal
 * Date: May 03, 2015
 * Description: 
 * Input: 
 * Output: 
 *****************************************************************************/

#ifndef UPPER_FILTER_HPP
#define UPPER_FILTER_HPP

#include "iFileFilter.hpp"
#include <fstream>

class UpperFilter : virtual public IFileFilter {
    public:
        void doFilter(std::ifstream &in, std::ofstream &out);
        char transform(char ch);
};

#endif
