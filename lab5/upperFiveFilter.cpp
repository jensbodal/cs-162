/******************************************************************************
 * Filename: upperFiveFilter.cpp
 * Author: Jens Bodal
 * Date: May 03, 2015
 * Description: 
 * Input: 
 * Output: 
 *****************************************************************************/

#include "upperFiveFilter.hpp"
#include <fstream>

void UpperFiveFilter::doFilter(std::ifstream &in, std::ofstream &out) {
    IFileFilter::doFilter(in, out);
}

char UpperFiveFilter::transform(char ch) {
    return std::toupper(ch);
}
