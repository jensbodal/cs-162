/******************************************************************************
 * Filename: iFileFilter.cpp
 * Author: Jens Bodal
 * Date: May 03, 2015
 * Description: default implementation for file filter interface methods
 *****************************************************************************/

#include "iFileFilter.hpp"
#include <fstream>


/***********************************************************************
 * Description: default implemenation, simply iterates through the file and
 * calls the transform function on each character
 * Parameters: the inputstream and the outputstream
 **********************************************************************/
void IFileFilter::doFilter(std::ifstream &in, std::ofstream &out) {
    while (in.good()) {
        char c = in.get();
        if (!in.eof()) {
            out << transform(c);
        }
    }
}
