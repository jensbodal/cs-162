/******************************************************************************
 * Filename: upperFilter.cpp
 * Author: Jens Bodal
 * Date: May 03, 2015
 * Description: 
 * Input: 
 * Output: 
 *****************************************************************************/

#include "upperFilter.hpp"
#include <fstream>

void UpperFilter::doFilter(std::ifstream &in, std::ofstream &out) {
    IFileFilter::doFilter(in, out);
}

char UpperFilter::transform(char ch) {
    return std::toupper(ch);
}
