/******************************************************************************
 * Filename: originalFilter.cpp
 * Author: Jens Bodal
 * Date: May 03, 2015
 * Description: creates an unchanged copy of the original file
 *****************************************************************************/

#include "originalFilter.hpp"
#include <fstream>

void OriginalFilter::doFilter(std::ifstream &in, std::ofstream &out) {
    IFileFilter::doFilter(in, out);
}

char OriginalFilter::transform(char ch) {
    return ch;
}
