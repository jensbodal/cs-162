/******************************************************************************
 * Filename: iFileFilter.hpp
 * Author: Jens Bodal
 * Date: May 03, 2015
 * Description: abstract class which defines the base IFileFilter class
 *****************************************************************************/

#ifndef I_FILE_FILTER_HPP
#define I_FILE_FILTER_HPP

#include <fstream>

class IFileFilter {
    public:
        /***********************************************************************
         * Description: this method defines how the filter will run, has a
         * default implementation provided in the class file
         * Parameters: the inputStream and the outputStream
         **********************************************************************/
        virtual void doFilter(std::ifstream &in, std::ofstream &out) = 0;
        virtual char transform(char ch) = 0;
};

#endif
