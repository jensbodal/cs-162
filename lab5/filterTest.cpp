/******************************************************************************
 * Filename: filterTest.cpp
 * Author: Jens Bodal
 * Date: May 03, 2015
 * Description: this tests the file filter program filters
 *****************************************************************************/

#include "originalFilter.hpp"
#include "encryptFilter.hpp"
#include "upperFilter.hpp"
#include "upperFiveFilter.hpp"

#include <fstream>
#include <string>

int main() {
    OriginalFilter oFilter;
    EncryptFilter eFilter;
    UpperFilter uFilter;    
    UpperFiveFilter fiveFilter;

    std::string iFile = "sample.txt";
    std::string orgFile = "original.txt";
    std::string encryptedFile = "encrypted.txt";
    std::string upperFile = "upper.txt";
    std::string upperFiveFile = "upperFive.txt";

    std::ifstream inputStream(iFile.c_str());
    std::ofstream originalOut(orgFile.c_str());
    std::ofstream encryptedOut(encryptedFile.c_str());
    std::ofstream upperOut(upperFile.c_str());
    std::ofstream upperFiveOut(upperFiveFile.c_str());

    oFilter.doFilter(inputStream, originalOut);
    inputStream.close();
    
    inputStream.open(iFile.c_str());
    eFilter.doFilter(inputStream, encryptedOut);
    inputStream.close();
    
    inputStream.open(iFile.c_str());
    uFilter.doFilter(inputStream, upperOut);
    inputStream.close();
    
    inputStream.open(iFile.c_str());
    fiveFilter.doFilter(inputStream, upperFiveOut);
    inputStream.close();


    return 0;
}
