/******************************************************************************
 * Filename: encryptFilter.cpp
 * Author: Jens Bodal
 * Date: May 03, 2015
 * Description: applies an encryption to the file contents; prompts the user to
 * enter the encryption integer
 *****************************************************************************/

#include "encryptFilter.hpp"
#include <fstream>
#include <iostream>
#include <limits>

EncryptFilter::EncryptFilter() {
    promptForEncryptionKey();
}

void EncryptFilter::promptForEncryptionKey() {
    int ourKey;
    bool ask = true;
    std::cout << std::endl;
    std::cout << "Enter an integer to use as the encryption key: ";
    while (ask) {
        std::cin >> ourKey;
        if (std::cin.fail() || std::cin.peek() != '\n') {
            std::cin.clear();
            std::cin.ignore(std::numeric_limits<int>::max(), '\n');
            std::cout << "Not an integer, try again: ";
        }
        else {
            ask = false;
        }
    }
    encryptionKey = ourKey;
}

void EncryptFilter::doFilter(std::ifstream &in, std::ofstream &out) {
    IFileFilter::doFilter(in, out);
}

char EncryptFilter::transform(char ch) {
    char returnChar = ch; 
    if (std::isalpha(ch)) {
        returnChar = (ch + encryptionKey);
        if (std::isupper(ch)) {
            returnChar %= 'A';
            returnChar %= 26;
            returnChar += 'A';
        }

        else {
            returnChar %= 'a';
            returnChar %= 26;
            returnChar += 'a';
        }
    }
    return returnChar;
}
