/******************************************************************************
 * Filename: item.cpp
 * Author: Jens Bodal
 * Date: April 26, 2015
 * Description: 
 *****************************************************************************/

#include "item.hpp"
#include <iomanip>
#include <string>
#include <sstream>

Item::Item(std::string name, std::string unit, int numToBuy, double unitPrice) {
    setName(name);
    setUnit(unit);
    setNumToBuy(numToBuy);
    setUnitPrice(unitPrice);
}

double Item::getTotalCost() {
    return (double)getNumToBuy() * getUnitPrice();
}

std::string Item::toString() {
    std::stringstream ss;
    ss << std::setprecision(2) << std::fixed;
    ss << getName() << " [" << getNumToBuy() << " @ ";
    ss << "$" << getUnitPrice() << "/" << getUnit() << "]: ";
    ss << "$" << getTotalCost();
    return ss.str();
}

void Item::setUnitPrice(double price) {
    int roundedPrice = price * 100 + 0.5;
    this->unitPrice = roundedPrice / 100.00;
}

bool Item::equals(Item *item) {
    std::string name1 = this->getName();
    std::string name2 = item->getName();
    
    for (int i = 0; i < name1.length(); i++) {
        name1[i] = tolower(name1[i]);
    }

    for (int i = 0; i < name2.length(); i++) {
        name2[i] = tolower(name2[i]);
    }

    return (name1 == name2);
}
