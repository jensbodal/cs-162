/******************************************************************************
 * Filename: clubItem.hpp
 * Author: Jens Bodal
 * Date: April 26, 2015
 * Description: defines a ClubItem object for a shopping program
 *****************************************************************************/

#ifndef CLUB_ITEM_HPP
#define CLUB_ITEM_HPP

#include "item.hpp"

class ClubItem : public Item { 
    public:
        /***********************************************************************
         * Description: creates a ClubItem object by calling the parent
         * constructor for an item object, then additionally discounts the price 
         * based on a defined discount value
         * Parameters: string name, string unit type, int number to buy, double
         * unit price (before discount is applied)
         **********************************************************************/
        ClubItem(std::string name, std::string unit, int numToBuy, double unitPrice);
};

#endif
