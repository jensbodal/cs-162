/******************************************************************************
 * Filename: item.hpp
 * Author: Jens Bodal
 * Date: April 26, 2015
 * Description: Defines an Item object for a shopping program
 *****************************************************************************/

#ifndef ITEM_HPP
#define ITEM_HPP

#include <string>

class Item {
    private:
        std::string name;
        std::string unit;
        int numToBuy;
        double unitPrice;
        void setName(std::string name) { this->name = name; }
        void setUnit(std::string unit) { this->unit = unit; }
    public:
        /***********************************************************************
         * Description: Creates an Item object with the specified name, unit,
         * number to buy, and unit price
         * Parameters: string name, string unit type, int number to buy, double
         * unit price
         **********************************************************************/
        Item(std::string name, std::string unit, int numToBuy, double unitPrice);

        std::string getName() { return this->name; }
        std::string getUnit() { return this->unit; }
        void setNumToBuy(int numToBuy) { this->numToBuy = numToBuy; }
        int getNumToBuy() { return this->numToBuy; }

        /***********************************************************************
         * Description: sets the unit price, rounding the value to the nearest
         * 10s place
         * Parameters: the price to set the unit to
         **********************************************************************/
        void setUnitPrice(double price);
        double getUnitPrice() { return this->unitPrice; }

        /***********************************************************************
         * Description: calculates the total cost of the item based on the
         * number of units to buy multiplied by the price per unit
         * Parameters: none
         **********************************************************************/
        double getTotalCost();
        
        /***********************************************************************
         * Description: returns a formatted and descriptive string of the item
         * Parameters: none
         **********************************************************************/
        std::string toString();

        /***********************************************************************
         * Description: returns true if the lowercase version of another
         * object's name matches the lowercase version of this objects name
         * Parameters: a pointer to the other object to compare to
         **********************************************************************/
        bool equals(Item* item);
};

#endif
