/******************************************************************************
 * Filename: shoppingProgram.cpp
 * Author: Jens Bodal
 * Date: April 26, 2015
 * Description: this program drives a shopping cart program which uses an Item
 * and List class to add and remove items from the cart
 * Input: items to add or remove to shopping carts
 * Output: various information regarding the items in the shopping cart and
 * status messages
 *****************************************************************************/

#include "shoppingProgram.hpp"
#include "item.hpp"
#include "clubItem.hpp"
#include "shoppingList.hpp"

#include <limits>
#include <iostream>

int main() {
    ShoppingProgram program;
    program.startShopping();
}

ShoppingProgram::ShoppingProgram() {
    shopping = true;
    std::cout << "\n\nWelcome to the Shopping Program!\n";
}

void ShoppingProgram::startShopping() {
    std::string yesNo;
    std::cout << "\nWe are creating a new list for you\n\n";
    delete list;
    list = NULL;
    list = new ShoppingList;
    std::cout << "Are you a club member? (Y/N): ";
    std::cin >> yesNo;
    if (yesNo == "Y" || yesNo == "y") {
        std::cout << "\nYou are a club member! Thank you!\n\n";
        isClubMember = true;
    }
    else {
        std::cout << "\nWe are sorry that you are not a club member\n";
        std::cout << "If you would like to sign up, please make the selection";
        std::cout << " to delete this list and create a new one.\n\n";
        isClubMember = false;
    }
    printMenu();
}

void ShoppingProgram::printMenu() {
    int selection;
    while (shopping) {
        std::cout << "Please make a selection: \n";
        std::cout << "  1. Add an item to your shopping list\n";
        std::cout << "  2. Remove an item from your shopping list\n";
        std::cout << "  3. Display the contents of your shopping list including";
        std::cout << " the total\n";
        std::cout << "  4. Delete this list create a new one\n";
        std::cout << "  5. Quit\n";
        std::cout << "    selection: ";
        selection = getInteger();

        switch(selection) {
            case 1:
                addItem();
                break;
            case 2:
                removeItem();
                break;
            case 3:
                printList();
                break;
            case 4:
                startShopping();
                break;
            case 5:
                quit();
                break;
            default:
                break;
        }
    }
}

int ShoppingProgram::getInteger() {
    int number;
    bool ask = true;
    while (ask) {
        std::cin >> number;
        if (std::cin.fail() || std::cin.peek() != '\n') {
            std::cin.clear();
            std::cin.ignore(std::numeric_limits<int>::max(), '\n');
            std::cout << "Invalid integer value, try again: ";
        }
        else {
            ask = false;
        }
    }
    return number;
}

double ShoppingProgram::getDouble() {
    double number;
    bool ask = true;
    while (ask) {
        std::cin >> number;
        if (std::cin.fail() || std::cin.peek() != '\n') {
            std::cin.clear();
            std::cin.ignore(std::numeric_limits<int>::max(), '\n');
            std::cout << "Invalid double value, try again: ";
        }
        else {
            ask = false;
        }
    }
    return number;
}

void ShoppingProgram::addItem() {
    std::string name;
    std::string unit;
    int quantity;
    double price;
    bool isOnSale = false;
    std::string yesNo;

    std::cout << "Item name: ";
    std::cin >> name;
    std::cout << "Unit type (bag, can, ounce, pound, etc.): ";
    std::cin >> unit;
    std::cout << "Number to purchase: ";
    quantity = getInteger();
    std::cout << "Price per unit: ";
    price = getDouble();
    if (isClubMember) {
        std::cout << "Is this item on sale? (Y/N): ";
        std::cin >> yesNo;
        if (yesNo == "Y" || yesNo == "y") {
            isOnSale = true;
            list->addItem(new ClubItem(name, unit, quantity, price));
        }
    }
    if (!isOnSale) {
        list->addItem(new Item(name, unit, quantity, price));
    }
    std::cout << "Item added to cart.\n";
    printList();
}

void ShoppingProgram::removeItem() {
    std::string name;
    std::cout << "Enter the name of the item to remove: ";
    std::cin >> name;
    list->removeItem(list->getItem(name));
    printList();
}

void ShoppingProgram::printList() {
    std::cout << '\n' << list->toString() << '\n' << '\n';
}

void ShoppingProgram::quit() {
    shopping = false;
}
