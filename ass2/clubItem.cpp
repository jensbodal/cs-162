/******************************************************************************
 * Filename: clubItem.cpp
 * Author: Jens Bodal
 * Date: April 26, 2015
 * Description: A ClubItem is an Item object that has a discounted price
 *****************************************************************************/

#include "clubItem.hpp"

const double DISCOUNT = 0.10;

ClubItem::ClubItem(
        std::string name, std::string unit, int numToBuy, double unitPrice) :
        Item(name, unit, numToBuy, unitPrice) {

    setUnitPrice(unitPrice - (unitPrice*DISCOUNT));
}
