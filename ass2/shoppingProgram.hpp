/******************************************************************************
 * Filename: shoppingProgram.hpp
 * Author: Jens Bodal
 * Date: April 26, 2015
 * Description: defines the view for a shopping program which uses the Item and
 * List classes toadd and remove items to the shopping cart
 *****************************************************************************/

#ifndef SHOPPING_PROGRAM_HPP
#define SHOPPING_PROGRAM_HPP

#include "shoppingList.hpp"

class ShoppingProgram {
    private:
        bool shopping;
        bool isClubMember;
        ShoppingList* list;
        void printMenu();
        int getInteger();
        double getDouble();
        void addItem();
        void removeItem();
        void printList();
        void quit();
    public:
        /***********************************************************************
         * Description: creates a new instance of the shopping program and sets
         * the shopping variable to true
         * Parameters: none
         **********************************************************************/
        ShoppingProgram();

        /***********************************************************************
         * Description: starts the program by printing the menu and creating a
         * new list
         * Parameters: none
         **********************************************************************/
        void startShopping();
};

#endif
