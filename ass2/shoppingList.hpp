/******************************************************************************
 * Filename: shoppingList.hpp
 * Author: Jens Bodal
 * Date: April 26, 2015
 * Description: Defines a ShoppingList object which contains a list of Items
 *****************************************************************************/

#ifndef SHOPPING_LIST_HPP
#define SHOPPING_LIST_HPP

#include "item.hpp"
#include <string>
#include <vector>

class ShoppingList {
    private:
        std::vector<Item*> items;
    public:

        /***********************************************************************
         * Description: creates a ShoppingList object
         * Parameters: none
         **********************************************************************/
        ShoppingList();

        /***********************************************************************
         * Description: adds an item to our ShoppingList object, return true if
         * item is added to list (case insensitive name match)
         * Parameters: the item to add to the list
         **********************************************************************/
        bool addItem(Item* item);

        /***********************************************************************
         * Description: returns a pointer to an item object if found in the
         * array, otherwise returns and empty item object
         * Parameters: the name of the item to search for
         **********************************************************************/
        Item* getItem(std::string itemName);

        /***********************************************************************
         * Description: removes an item from our ShoppingList object, returns
         * true if item is removed from list (case insensitive name match)
         * Parameters: the item to remove from the list
         **********************************************************************/
        bool removeItem(Item* item);

        /***********************************************************************
         * Description: returns a formatted string of the contents of our
         * shopping cart including the total
         * Parameters: none
         **********************************************************************/
        std::string toString();

        /***********************************************************************
         * Description: returns the total cost of all items in the list
         * Parameters: none
         **********************************************************************/
        double totalCost();
};

#endif
