/******************************************************************************
 * Filename: shoppingList.cpp
 * Author: Jens Bodal
 * Date: April 26, 2015
 * Description: This class tracks a list of items that a user wants to purchase
 * and allows them to add/remove items from the list and to see its contents
 *****************************************************************************/

#include "shoppingList.hpp"
#include "item.hpp"

#include <iomanip>
#include <string>
#include <sstream>
#include <vector>

ShoppingList::ShoppingList() {
    // empty constructor
}

bool ShoppingList::addItem(Item* item) {
    for (int i = 0; i < items.size(); i++) {
        if (items.at(i)->equals(item)) {
            return false;
        }
    }
    items.push_back(item);
    return true;
}

Item* ShoppingList::getItem(std::string itemName) {
    Item* lookItem = new Item(itemName, "", 0, 0);
    for (int i = 0; i < items.size(); i++) {
        if (items.at(i)->equals(lookItem)) {
            return items.at(i);
        }
    }
    return lookItem;
}

bool ShoppingList::removeItem(Item* item) {
    for (int i = 0; i < items.size(); i++) {
        if (items.at(i)->equals(item)) {
            items.erase(items.begin()+i);
            // only one item with this name exists, so exit after it's removed
            return true;
        }
    }
    return false;
}

std::string ShoppingList::toString() {
    std::stringstream contents;
    contents << std::fixed << std::setprecision(2);
    for (int i = 0; i < items.size(); i++) {
        contents << items.at(i)->toString() << '\n';
    }
    contents << "Total Cost: $" << totalCost();
    return contents.str();

}

double ShoppingList::totalCost() {
    double total = 0;
    for (int i = 0; i < items.size(); i++) {
        total+=items.at(i)->getTotalCost();
    }

    return total;
}
