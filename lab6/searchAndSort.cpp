/******************************************************************************
 * Filename: searchAndSort.cpp
 * Author: Jens Bodal
 * Date: May 07, 2015
 * Description: this program will search and sort the input file specified in
 * main
 * Input: various files hardcoded in main
 * Output: target location of target value and output files for sorted values
 *****************************************************************************/

#include "searchAndSort.hpp"

#include <fstream>
#include <iostream>
#include <string>
#include <vector>

int main() {
    std::string randBase = "randBase.txt";
    std::string randEarly = "randEarly.txt";
    std::string randMiddle = "randMiddle.txt";
    std::string randLate = "randLate.txt";
    std::ifstream baseInput;
    std::ifstream earlyInput;
    std::ifstream middleInput;
    std::ifstream lateInput;

    std::string filePrefix = "";
    std::ofstream outputFile;
    bool result;

    SearchAndSort instance;
    int target = 0;

    std::cout << "\n\n";

    // use linear search
    std::cout << "Search Method 1 (no binary search tree)\n";
    std::cout << "  Searching for: " << target << "\n";
    
    // base
    std::cout << "  Base file target location: ";
    baseInput.open(randBase.c_str());
    std::cout << instance.searchTarget(baseInput, target) << '\n';
    baseInput.close();

    // early
    std::cout << "  Early file target location: ";
    earlyInput.open(randEarly.c_str());
    std::cout << instance.searchTarget(earlyInput, target) << '\n';
    earlyInput.close();
    
    // middle
    std::cout << "  Middle file target location: ";
    middleInput.open(randMiddle.c_str());
    std::cout << instance.searchTarget(middleInput, target) << '\n';
    middleInput.close();
    
    // late
    std::cout << "  Late file target location: ";
    lateInput.open(randLate.c_str());
    std::cout << instance.searchTarget(lateInput, target) << '\n';
    lateInput.close();
    std::cout << "\n\n";

    // use binary search tree
    std::cout << "Search Method 2 (USING binary search tree)\n";
    std::cout << "  Searching for: " << target << "\n";
   
    // base
    std::cout << "  Base file target location: ";
    baseInput.open(randBase.c_str());
    std::cout << instance.binarySearchTarget(baseInput, target) << '\n';
    baseInput.close();

    // early
    std::cout << "  Early file target location: ";
    earlyInput.open(randEarly.c_str());
    std::cout << instance.binarySearchTarget(earlyInput, target) << '\n';
    earlyInput.close();
  
    // middle
    std::cout << "  Middle file target location: ";
    middleInput.open(randMiddle.c_str());
    std::cout << instance.binarySearchTarget(middleInput, target) << '\n';
    middleInput.close();
    
    // late
    std::cout << "  Late file target location: ";
    lateInput.open(randLate.c_str());
    std::cout << instance.binarySearchTarget(lateInput, target) << '\n';
    std::cout << "\n\n";
    lateInput.close();


    std::cout << "Now sorting all of the files\n";
    std::cout << "Please enter a string to prepend to the file names: ";
    std::cin >> filePrefix;
    filePrefix += "-"; // add dash between prefix and file name

    // Sorting base file
    outputFile.open((filePrefix+randBase).c_str());
    baseInput.open(randBase.c_str());
    result = instance.sortValues(baseInput, outputFile);
    baseInput.close();
    instance.printSortResult(randBase, result);
    std::cout << '\n';
    outputFile.close();

    // Sorting early file
    outputFile.open((filePrefix+randEarly).c_str());
    earlyInput.open(randEarly.c_str());
    result = instance.sortValues(earlyInput, outputFile);
    earlyInput.close();
    instance.printSortResult(randEarly, result);
    std::cout << '\n';
    outputFile.close();
    
    // Sorting middle file
    outputFile.open((filePrefix+randMiddle).c_str());
    middleInput.open(randMiddle.c_str());
    result = instance.sortValues(middleInput, outputFile);
    middleInput.close();
    instance.printSortResult(randMiddle, result);
    std::cout << '\n';
    outputFile.close();
    
    // Sorting late file
    outputFile.open((filePrefix+randLate).c_str());
    lateInput.open(randLate.c_str());
    result = instance.sortValues(lateInput, outputFile);
    lateInput.close();
    instance.printSortResult(randLate, result);
    std::cout << '\n';
    outputFile.close();
    
    return 0;
}

SearchAndSort::SearchAndSort() {
    // empty default constructor;
}

int SearchAndSort::searchTarget(std::ifstream &inputS, int target) {
    int currentPlace = 0;
    while(inputS) {
        currentPlace++;
        int currentInt;
        inputS >> currentInt;
        if (currentInt == target) { return currentPlace; }
    }
    
    return -1;
}

bool SearchAndSort::sortValues(std::ifstream &inputS, std::ofstream &outputS) {
    std::vector<int> inputNumbers;
    int temp;
    bool swap = false;

    while (inputS) {
        int number;
        inputS >> number;
        if (inputS.peek() != -1) {
            inputNumbers.push_back(number);
        }
    }

    // bubble sort
    do {
        swap = false;
        for (int count = 0; count < inputNumbers.size() - 1; count++) {
            if (inputNumbers[count] > inputNumbers[count + 1]) {
                temp = inputNumbers[count];
                inputNumbers[count] = inputNumbers[count + 1];
                inputNumbers[count + 1] = temp;
                swap = true;
            }
        }
    }
    while(swap);

    for (int i = 0; i < inputNumbers.size(); i++) {
        outputS << inputNumbers[i] << '\n';
    }
    
    return !swap;
}

int SearchAndSort::binarySearchTarget(std::ifstream &inputS, int target) {
    std::string tempFileName(".temp");
    std::ofstream tempFile(tempFileName.c_str());
    std::ifstream newFile;
    std::vector<int> numbers;
    int first;          // first element
    int middle;         // midpoint of search
    int last;           // last element
    int position;       // position of search value
    bool found;         // flag
    
    // sort input file
    sortValues(inputS, tempFile);
    tempFile.close();

    // write input file to vector
    newFile.open(tempFileName.c_str());
    while (newFile) {
        int number;
        newFile >> number;
        if (newFile.peek() != -1) {
            numbers.push_back(number);
        }
    }
    newFile.close();

    // binary search tree
    int size = numbers.size();
    first = 0;
    last = size - 1;
    position = -1;
    found = false;

    while (!found && first <= last) {
        middle = (first + last) / 2;            // calcualte midpoint
        if (numbers[middle] == target) {        // if value is found at mid
            found = true;
            position = middle;
        }
        else if (numbers[middle] > target) {    // if target is in lower half
            last = middle - 1;
        }
        else {
            first = middle + 1;                 // if target is in upper half
        }

    }

    return position;
}

void SearchAndSort::printSortResult(std::string fileName, bool result) {
    std::cout << "[Filename: " << fileName << "] Result: ";
    if (result) {
        std::cout << "SUCCESS";
    }
    else {
        std::cout << "FAIL";
    }
}

