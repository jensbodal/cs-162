/******************************************************************************
 * Filename: searchAndSort.hpp
 * Author: Jens Bodal
 * Date: May 07, 2015
 * Description: defines the characteristics of a searching and sorting program
 *****************************************************************************/

#ifndef SEARCH_AND_SORT_HPP
#define SEARCH_AND_SORT_HPP

#include <fstream>
#include <string>

class SearchAndSort {
    public:
        SearchAndSort();

        /***********************************************************************
         * Description: searches the input file stream for the target value and
         * return the line number or position for which it occurs
         * Parameters: the inputfilestream reference, the target value
         **********************************************************************/
        int searchTarget(std::ifstream &inputS, int target);

        /***********************************************************************
         * Description: takes the input file stream and outputs it to the output
         * file stream sorted from smallest to largest
         * Parameters: the inputfile stream reference, the outputfilestream
         * reference
         **********************************************************************/
        bool sortValues(std::ifstream &inputS, std::ofstream &outputS);

        /***********************************************************************
         * Description: searches the input file stream for the target value and
         * return the line number or position for which it occurs; utilizes a
         * binary search tree
         * Parameters: the inputfilestream reference, the target value
         **********************************************************************/
        int binarySearchTarget(std::ifstream &inputS, int target);

        /***********************************************************************
         * Description: prints a string message describing the result of the
         * sort
         * Parameters: string of the file name, bool value for success or not
         **********************************************************************/
        void printSortResult(std::string fileName, bool result);
};

#endif
