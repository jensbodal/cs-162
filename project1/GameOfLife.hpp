/******************************************************************************
 * Filename: GameOfLife.hpp
 * Author: Jens Bodal
 * Date: April 02, 2015
 * Description: describes the functionality of the Game Of Life program
 *****************************************************************************/

#ifndef GAMEOFLIFE_HPP
#define GAMEOFLIFE_HPP

#include "Board.hpp"
#include <queue>
#include <string>

class GameOfLife {
    private:
        /***********************************************************************
         * Description: Holds the Board object for the game
         * Parameters: none
         **********************************************************************/
        Board *board;

        /***********************************************************************
         * Description: delay in microseconds in which to update the game
         **********************************************************************/
        int delay;
        
        /***********************************************************************
         * Description: number of times to simulate the game
         **********************************************************************/
        int iterations;

        /***********************************************************************
         * Description: holds the list of coordinates that will be used to
         * update the state of the board
         **********************************************************************/
        std::queue<Board::Coordinates> updateList;
 
        /***********************************************************************
         * Description: iterates through the update list and changes the needed
         * values of all of the spaces on the board
         * Parameters: none
         **********************************************************************/
        void switchValues();
        
        /***********************************************************************
         * Description: used to adjust for the displayed game window 
         **********************************************************************/
        int offset;
        
        /***********************************************************************
         * Description: sets the delay used for time between board updates
         * Parameters: time in milliseconds to use as the update interval
         **********************************************************************/
        void setDelay(int milliseconds);
        
        /***********************************************************************
         * Description: sets the number of times the simulation will run
         * Parameters: integer value for number of iterations
         **********************************************************************/
        void setIterations(int iterations);
    public:
        /***********************************************************************
         * Description: creates a new game of life object
         * Parameters: the board to use 
         **********************************************************************/
        GameOfLife(Board *board);
        
        /***********************************************************************
         * Description: starts the board simulation of updating pieces
         * Parameters: number of times to change values
         **********************************************************************/
        void run(int iterations);
        
        /***********************************************************************
         * Description: prints the game start menu
         * Parameters: none
         **********************************************************************/
        void printMenu();
        
        /***********************************************************************
         * Description: obtains the delay used for time between updates
         * Parameters: none
         **********************************************************************/
        int getDelay();
        
        /***********************************************************************
         * Description: obtains an integer value from the user
         * Parameters: the message to print when asking for a value
         **********************************************************************/
        int getInteger(std::string message);
        
        /***********************************************************************
         * Description: prompts the user to select a shape to use
         * Parameters: none
         **********************************************************************/
        void selectShape();
        
        /***********************************************************************
         * Description: prints the current game board
         * Parameters: none
         **********************************************************************/
        void printBoard();
        
        /***********************************************************************
         * Description: clears the game board
         * Parameters: none
         **********************************************************************/
        void clearBoard();
        
        /***********************************************************************
         * Description: iterates through the board and determines which spaces
         * need to be added to the updateQueue for the next  board update 
         * Parameters: none
         **********************************************************************/
        void updateBoard();
        
        /***********************************************************************
         * Description: checks the space on the board to determine if needs to
         * be updated
         * Parameters: the coordinates for the space to check
         **********************************************************************/
        bool shouldUpdate(Board::Coordinates coords);
        
        /***********************************************************************
         * Description: prompts the user to create a new shape
         * Parameters: none
         **********************************************************************/
        void createCustomShape();
        
        /***********************************************************************
         * Description: creates a 9x9 square to add to the board
         * Parameters: the starting x and y coordinates
         **********************************************************************/
        void add9x9Square(int startX, int startY);
        
        /***********************************************************************
         * Description: creates a blinker shape to add to the board
         * Parameters: the starting x and y coordinates
         **********************************************************************/
        void addBlinker(int startX, int startY);
        
        /***********************************************************************
         * Description: creates a beacon shape to add to the board
         * Parameters: the starting x and y coordinates
         **********************************************************************/
        void addBeacon(int startX, int startY);
        
        /***********************************************************************
         * Description: creates a glider shape to add to the board
         * Parameters: the starting x and y coordinates
         **********************************************************************/
        void addGlider(int startX, int startY);
        
        /***********************************************************************
         * Description: creates a simple shape to add to the board
         * Parameters: the starting x and y coordinates
         **********************************************************************/
        void addSimpleShape(int startX, int startY);

        /***********************************************************************
         * Description: adds a GosperGlider to the board
         * Parameters: the starting x and y coordinates
         **********************************************************************/
        void addGosperGlider(int startX, int startY);
        
        /***********************************************************************
         * Description: fun method to call to add a unique arrangement of shapes
         * to the board at specified x and y coordinates
         * Parameters: none
         **********************************************************************/
        void beCrazy();
};

#endif
