/******************************************************************************
 * Filename: GameOfLife.cpp
 * Author: Jens Bodal
 * Date: April 12, 2015
 * Description: this implements the game of life program then has an example
 * main method of how to run it. 
 * Input: user input prompted as necessary
 * Output: the output of the game as it runs
 *****************************************************************************/

#include "Board.hpp"
#include "GameSpace.hpp"
#include "GameOfLife.hpp"

#include <limits>
#include <iostream>
#include <queue>
#include <sstream>

const int DISPLAY_WIDTH = 40;
const int DISPLAY_HEIGHT = 20;
const int DEFAULT_DELAY = 80;
const int DEFAULT_ITERATIONS = 50;

int main() {
    Board board(DISPLAY_WIDTH, DISPLAY_HEIGHT);
    GameOfLife game(&board);
    game.printMenu();
}

GameOfLife::GameOfLife(Board *board) {
    this->board = board;
    offset = board->getBuffer() / 2;
    this->setDelay(DEFAULT_DELAY);
    this->setIterations(DEFAULT_ITERATIONS);
}

void GameOfLife::printMenu() {
    bool running = true;
    int selection;

    std::cout << "Welcome to Conway's Game Of Life!\n";
    std::cout << "    implemented by: Jens Bodal\n\n";

    while (running) {
        std::cout << "Please make a selection \n";
        std::cout << "    1. Adjust delay (Current: " << getDelay() << " ms)\n";
        std::cout << "    2. Set number of iterations (Current: ";
            std::cout << iterations << ")\n";
        std::cout << "    3. Add a default shape\n";
        std::cout << "    4. Add a custom shape\n";
        std::cout << "    5. !!! Start the program !!!\n";
        std::cout << "    6. Print the board\n";
        std::cout << "    7. Clear the board\n";
        std::cout << "    8. Do something crazy!\n";
        std::cout << "    9. Quit\n\n";
        selection = getInteger("selection");

        switch (selection) {
            // adjust delay
            case 1: 
                std::cout << "Enter the delay in milliseconds\n";
                setDelay(getInteger("delay"));
                break;
            // set iterations
            case 2:
                std::cout << "Enter the number of times to loop";
                std::cout << " (recommended: >50)\n";
                setIterations(getInteger("iterations"));
                break;
            // add a default shape
            case 3:
                selectShape();
                break;
            // custom shape
            case 4:
                createCustomShape();
                break;
            // start the program
            case 5:
                this->run(iterations);
                break;
            // print board
            case 6:
                this->printBoard();
                break;
            // clear the board
            case 7:
                this->clearBoard();
                break;
            // be crazy
            case 8: 
                beCrazy();
                break;
            // quit
            case 9:
                running = false;
                break;
            default: 
                break;
        }
    }
}

void GameOfLife::selectShape() {
    int selection;

    std::cout << "Select the shape that you want to add\n";
    std::cout << "    1. 9x9 Square\n";
    std::cout << "    2. Blinker\n";
    std::cout << "    3. Beacon\n";
    std::cout << "    4. Glider\n";
    std::cout << "    5. Gosper Glider Gun\n";
    std::cout << "    6. A Simple Shape\n";
    selection = getInteger("Shape #");

    int row;
    int col;
    std::cout << "Starting row (recommended: 10 | max: " << DISPLAY_HEIGHT -1;
        std::cout << ")\n";
    row = getInteger("  Row");
    std::cout << "Startin column (recommended: 0 | max: ";
        std::cout << DISPLAY_WIDTH-1 << ")\n";
    col = getInteger("  Column");

    switch (selection) {
        case 1:
            this->add9x9Square(row, col);
            break;
        case 2:
            this->addBlinker(row, col);
            break;
        case 3:
            this->addBeacon(row, col);
            break;
        case 4:
            this->addGlider(row, col);
            break;
        case 5:
            std::cout << "\nRecommended starting row of 5 and column 0\n";
            this->addGosperGlider(row, col);
            
            break;
        case 6:
            this->addSimpleShape(row, col);
            break;
    }

    this->printBoard();
}

int GameOfLife::getInteger(std::string message) {
    bool gettingSelection = true;
    int selection;

    while (gettingSelection) {
        std::cout << message << ": ";
        std::cin >> selection;
        if (std::cin.fail() || std::cin.peek() != '\n') {
            std::cin.clear();
            std::cin.ignore(std::numeric_limits<int>::max(), '\n');
            std::cout << "Invalid selection, try again\n\n";
        }
        else {
            std::cout << '\n';
            gettingSelection = false;
        }
    }   

    return selection;
}

void GameOfLife::setDelay(int milliseconds) {
    this->delay = milliseconds * 1000;
}

int GameOfLife::getDelay() {
    return this->delay / 1000;
}

void GameOfLife::setIterations(int iterations) {
    this->iterations = iterations;
}

void GameOfLife::run(int iterations) {
    int i = 0;
    while (i < iterations) {
        usleep(delay);
        this->updateBoard();
        this->printBoard();
        i++;
        std::cout << "\nPress control+c to stop early\n";
        std::cout << "Iterations: " << i << " (" << iterations << ")" << '\n';
    }
}

void GameOfLife::printBoard() {
    std::stringstream boardString;
    boardString << std::string(50, '\n');
    int rows = board->getRows();
    int cols = board->getColumns();
    for (int row = offset; row < offset + DISPLAY_HEIGHT; row++) {
        for (int col = offset; col < offset + DISPLAY_WIDTH; col++) {
            Board::Coordinates coords;
            coords.x = row;
            coords.y = col;
            boardString << board->getSpace(coords)->getValue();
        }
        boardString << '\n';
    }
    std::cout << boardString.str();
    std::cout << std::endl;
}

void GameOfLife::clearBoard() {
    int rows = board->getRows();
    int cols = board->getColumns();
    for (int row = 0; row < rows; row++) {
        for (int col = 0; col < cols; col++) {
            board->setEmpty(row, col);
        }
    }
}

void GameOfLife::updateBoard() {
    int rows = board->getRows();
    int cols = board->getColumns();
    for (int row = 0; row < rows; row++) {
        for (int col = 0; col < cols; col++) {
            Board::Coordinates coords;
            coords.x = row;
            coords.y = col;
    
            if (shouldUpdate(coords)) {
                updateList.push(coords);
            }   
        }
    }
    switchValues();
}

bool GameOfLife::shouldUpdate(Board::Coordinates coords) {
    GameSpace *space = board->getSpace(coords);
    int neighbors = 0;

    // check all neighboring cells to see how many neighbors we have
    for (int i = 1; i <= 8; i++) {
        if (board->isActiveNeighbor(&coords, i)) {
            neighbors++;
        }
    }

    /*
     * If an occupied cell has zero, one, or more than three neighbors, then it
     * dies of loneliness or overcrowding
     */
    if (!space->isEmpty()) {
        if (neighbors == 0 || neighbors == 1 || neighbors > 3) {
            return true;
        }
    }

    /*
     * if an empty cell has exactly three neighbors, then it springs to life!!
     */
    if (space->isEmpty()) {
        if (neighbors == 3) {
            return true;
        }
    }

    return false;
}

void GameOfLife::switchValues() {
    int numberOfValues = updateList.size();
    for (int i = 0; i < numberOfValues; i++) {
        board->getSpace(updateList.front())->switchValue();
        updateList.pop();
    }
}

void GameOfLife::createCustomShape() {
    int row;
    int col;
    bool getEntry = true;
    int selection;

    while (getEntry) {
    std::cout << "To create a custom shape, enter the row and column";
    std::cout << " which you want to fill.\n\n";
    std::cout << "The board will print after each entry so you can see what";
    std::cout << " your shape looks like.\n";
    std::cout << "(Note: If the row/column is not visible then the space will";
    std::cout << " not become occupied)\n\n";

    std::cout << "Enter row and column\n";
    row = getInteger("Row");
    col = getInteger("Column");

    board->setOccupied(row+offset, col+offset);
    this->printBoard();

    std::cout << "\nAdd another? (1 = yes, 2 = no)";
    selection = getInteger("");
    
    if (selection != 1) { getEntry = false; }
    }   

}

void GameOfLife::add9x9Square(int startX, int startY) {
    startX+=offset;
    startY+=offset;
    board->setOccupied(startX,startY);
    board->setOccupied(startX,startY+1);
    board->setOccupied(startX,startY+2);
    board->setOccupied(startX+1,startY);
    board->setOccupied(startX+1,startY+1);
    board->setOccupied(startX+1,startY+2);
    board->setOccupied(startX+2,startY);
    board->setOccupied(startX+2,startY+1);
    board->setOccupied(startX+2,startY+2);
}

void GameOfLife::addBlinker(int startX, int startY) {
    startX+=offset;
    startY+=offset;
    board->setOccupied(startX,startY);
    board->setOccupied(startX,startY+1);
    board->setOccupied(startX,startY+2);
}

void GameOfLife::addBeacon(int startX, int startY) {
    startX+=offset;
    startY+=offset;
    board->setOccupied(startX,startY);
    board->setOccupied(startX+1,startY);
    board->setOccupied(startX,startY+1);
    board->setOccupied(startX+3,startY+2);
    board->setOccupied(startX+3,startY+3);
    board->setOccupied(startX+2,startY+3);
}

void GameOfLife::addGlider(int startX, int startY) {
    startX+=offset;
    startY+=offset;
    board->setOccupied(startX, startY);
    board->setOccupied(startX+1, startY);
    board->setOccupied(startX+2, startY);
    board->setOccupied(startX, startY+1);
    board->setOccupied(startX+1, startY+2);

}

void GameOfLife::addSimpleShape(int startX, int startY) {
    startX+=offset;
    startY+=offset;
    board->setOccupied(startX, startY);
    board->setOccupied(startX, startY+1);
    board->setOccupied(startX, startY+2);
    board->setOccupied(startX-1, startY+3);
    board->setOccupied(startX-2, startY+3);
}

void GameOfLife::addGosperGlider(int startX, int startY) {
    startX+=offset;
    startY+=offset;
    startX-=4; // need to adjust down for this particular shape
    board->setOccupied(startX, startY);
    board->setOccupied(startX, startY+1);
    board->setOccupied(startX+1, startY);
    board->setOccupied(startX+1, startY+1);

    startY=startY+10; // shift over to start next shape
    board->setOccupied(startX, startY);
    board->setOccupied(startX-1, startY+1);
    board->setOccupied(startX-2, startY+2);
    board->setOccupied(startX-2, startY+3);
    board->setOccupied(startX-1, startY+5);
    board->setOccupied(startX, startY+6);
    board->setOccupied(startX+1, startY+4);
    board->setOccupied(startX+1, startY+6);
    board->setOccupied(startX+1, startY+7);
    board->setOccupied(startX+2, startY+6);
    board->setOccupied(startX+3, startY+5);
    board->setOccupied(startX+4, startY+3);
    board->setOccupied(startX+4, startY+2);
    board->setOccupied(startX+3, startY+1);
    board->setOccupied(startX+2, startY);
    board->setOccupied(startX+1, startY);

    // shift to new starting point for next shape
    startX-=2;
    startY+=10;
    board->setOccupied(startX, startY);
    board->setOccupied(startX, startY+1);
    board->setOccupied(startX+1, startY);
    board->setOccupied(startX+1, startY+1);
    board->setOccupied(startX+2, startY);
    board->setOccupied(startX+2, startY+1);
    board->setOccupied(startX-1, startY+2);
    board->setOccupied(startX-1, startY+4);
    board->setOccupied(startX-2, startY+4);
    board->setOccupied(startX+3, startY+2);
    board->setOccupied(startX+3, startY+4);
    board->setOccupied(startX+4, startY+4);

    startY+=14;
    board->setOccupied(startX, startY);
    board->setOccupied(startX, startY+1);
    board->setOccupied(startX+1, startY);
    board->setOccupied(startX+1, startY+1);

    // adding glider
    startY-=11;
    startX+=7;
    board->setOccupied(startX, startY);
    board->setOccupied(startX+1, startY+1);
    board->setOccupied(startX+1, startY+2);
    board->setOccupied(startX+2, startY);
    board->setOccupied(startX+2, startY+1);
}

void GameOfLife::beCrazy() {
    this->clearBoard();
    this->add9x9Square(5,5);
    this->add9x9Square(10,10);
    this->addBlinker(18, 18);
    this->setIterations(100);
    this->run(iterations);
}
