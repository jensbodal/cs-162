/******************************************************************************
 * Filename: Board.hpp
 * Author: Jens Bodal
 * Date: April 02, 2015
 * Description: describes the functionality of a Board object to be used as a
 * GameBoard in the GameOfLife program
 *****************************************************************************/

#ifndef BOARD_HPP
#define BOARD_HPP

#include "GameSpace.hpp"
#include <vector>

class Board {
    private:
        typedef std::vector<std::vector<GameSpace> > GameSpaces;
        int rows;
        int cols;
        GameSpaces spaces; 
        static const int VIEW_BUFFER = 400;

    public:
        /***********************************************************************
         * Description: constructs a Board object with the specified number of
         * "visible" rows and columns; the board contains more space as defined
         * by the VIEW_BUFFER constant
         * Parameters: number of "visible" rows and columns for the board
         **********************************************************************/
        Board(int rows, int cols);

        /***********************************************************************
         * Description: coordinates object to aid in referencing spaces on the
         * game board
         **********************************************************************/
        struct Coordinates {
            int x;
            int y;
        };

        
        /***********************************************************************
         * Description: returns a pointer to a space on the game board specified
         * by the given coordinates
         * Parameters: coordinates of the space to return a pointer to
         **********************************************************************/
        GameSpace *getSpace(Coordinates coords);
        
        /***********************************************************************
         * Description: returns a pointer to a space on the game board specified
         * by the given x, y values for row and column.  
         * This is an overloaded method and converts the (row,column) pair to a
         * coordinates object
         * Parameters: (row, column) pair of the space to return a pointer to
         **********************************************************************/
        GameSpace *getSpace(int row, int col);
        
        /***********************************************************************
         * Description: returns the 2D vector array of GameSpaces which is used
         * to represent the GameBoard
         * Parameters: none
         **********************************************************************/
        GameSpaces getSpaces() { return spaces; }

        /***********************************************************************
        * Description: The neighbor number is defined as follows, where # is the
        * current cell:
        *              1 2 3
        *              8 # 4
        *              7 6 5
        * 
        * Parameters: a pointer to the GameSpace to check and the number of the
        * neighbor to see if currently active (not empty)
        ***********************************************************************/
        bool isActiveNeighbor(Coordinates *coords, int neighbor);
        
        /***********************************************************************
         * Description: forces a GameSpace (row, column) pair to be occupied
         * This is helpful when setting up a board with a default shape
         * Parameters: (row, column) pair of the space to be occupied
         **********************************************************************/
        void setOccupied(int row, int col);

        /***********************************************************************
         * Description: forces a GameSpace (row, column) pair to be occupied
         * This is helpful when setting up a board with a default shape
         * Parameters: (row, column) pair of the space to be occupied
         **********************************************************************/
        void setEmpty(int row, int col);
        
        /***********************************************************************
         * Description: returns the  VIEW_BUFFER of the board; the visible board
         * is essentially a "window" of the actual board size, which is
         * specified by the VIEW_BUFFER; the VIEW_BUFFER adds that amount to the
         * total number of rows and columns which are visible
         * Parameters: none
         **********************************************************************/
        int getBuffer();

        int getRows() { return rows; }
        int getColumns() { return cols; }
};

#endif
