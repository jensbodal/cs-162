/******************************************************************************
 * Filename: GameSpace.cpp
 * Author: Jens Bodal
 * Date: April 02, 2015
 * Description: object which defines a GameSpace object for the Board class
 * which exists for the GameOfLife program
 *****************************************************************************/

#include "GameSpace.hpp"

#include <string>

const std::string OCCUPIED = "\u25A0";
const std::string EMPTY = " "; 


GameSpace::GameSpace() {
    setValue(EMPTY);
}

void GameSpace::switchValue() {
    if (getValue() == OCCUPIED) {
        setValue(EMPTY);
    }
    else {
        setValue(OCCUPIED);
    }
}

bool GameSpace::isEmpty() {
    return (getValue() == EMPTY);
}
