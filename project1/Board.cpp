/******************************************************************************
 * Filename: Board.cpp
 * Author: Jens Bodal
 * Date: April 02, 2015
 * Description: this class represents a Board object to be used in the
 * GameOfLife program.  The board contains GameSpace objects that are contained
 * within a 2D vectory array.  The number of rows and columns of the board are
 * intended to be much larger than the "visible" number of rows and columns, so
 * when printing the board be sure to adjust the print output accordingly
 *****************************************************************************/

#include "Board.hpp"
#include "GameSpace.hpp"

#include <vector>


Board::Board(int rows, int cols) {
    /* 
     * adding buffer to total number of rows and columns, so that the user does
     * not need to be concerned with the actual number of rows and columns
     * contained within the board.  The larger the buffer the more time it takes
     * to iterate through all of the gamespaces
     */
    rows+=VIEW_BUFFER;
    cols+=VIEW_BUFFER;
    this->rows = rows;
    this->cols = cols;

    /*
     * to populate a 2D vector we need to first create a temporary vector which
     * will then be pushed onto each row of the 2D vector
     */
    for (int row = 0; row < rows; row++) {
        std::vector<GameSpace> temp;
        for (int col = 0; col < cols; col++) {
            temp.push_back(GameSpace());
        }
        spaces.push_back(temp);
    }
}    

GameSpace *Board::getSpace(Coordinates coords) {
    return &spaces[coords.x][coords.y];
}

GameSpace *Board::getSpace(int row, int col) {
    Coordinates coords;
    coords.x = row;
    coords.y = col;
    return getSpace(coords);
}

/*
 * in order to check the neighboring cells we need to first see if they exist or
 * not.  If they do exist then we need to see if they are occupied or not, if
 * they are occupied then we return true
 */
bool Board::isActiveNeighbor(Coordinates *coords, int neighbor) {
    // default value for neighbor is that it is inactive
    bool isActive = false;
    // tracks whether a neighbor exists or not
    bool outOfBounds = true;
    int currentRow = coords->x;
    int currentCol = coords->y;
    // 1, 2, 3 don't exist if at minRow
    bool atMinRow = (currentRow - 1 < 0) ? true : false;
    // 7, 6, 5 don't exist if at maxRow
    bool atMaxRow = (currentRow + 1 > getRows() - 1) ? true : false;
    // 1, 8, 7 don't exist if at minCol
    bool atMinCol = (currentCol - 1 < 0) ? true : false;
    // 3, 4, 5 don't exist if at maxCol
    bool atMaxCol = (currentCol + 1 > getColumns() - 1) ? true : false;
    
    switch (neighbor) {
        case 1:
            if (!atMinRow && !atMinCol) {
                outOfBounds = false;
                currentRow--;
                currentCol--;
            }
            break;
        case 2:
            if (!atMinRow) {
                outOfBounds = false;
                currentRow--;
            }
            break;
        case 3:
            if (!atMinRow && !atMaxCol) {
                outOfBounds = false;
                currentRow--;
                currentCol++;
            }
            break;
        case 4:
            if (!atMaxCol) {
                outOfBounds = false;
                currentCol++;
            }
            break;
        case 5:
            if (!atMaxRow && !atMaxCol) {
                outOfBounds = false;
                currentRow++;
                currentCol++;
            }
            break;
        case 6:
            if (!atMaxRow) {
                outOfBounds = false;
                currentRow++;
            }
            break;
        case 7:
            if (!atMaxRow && !atMinCol) {
                outOfBounds = false;
                currentRow++;
                currentCol--;
            }
            break;
        case 8:
            if (!atMinCol) {
                outOfBounds = false;
                currentCol--;
            }
            break;
        default:
            isActive = false;
            break;
    }
    
    if (!outOfBounds) {
        isActive = !getSpace(currentRow, currentCol)->isEmpty();
    }

    return isActive;
}

void Board::setOccupied(int row, int col) {
    if (row < 0 || row > this->rows || col < 0 || col > this->cols) {
        return;
    }
    GameSpace *space = getSpace(row, col);
    if (space->isEmpty()) {
        space->switchValue();
    }
}

void Board::setEmpty(int row, int col) {
    if (row < 0 || row > this->rows || col < 0 || col > this->cols) {
        return;
    }
    GameSpace *space = getSpace(row, col);
    if (!space->isEmpty()) {
        space->switchValue();
    }
}

int Board::getBuffer() {
    return VIEW_BUFFER;
}
