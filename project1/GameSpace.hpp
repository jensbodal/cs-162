/******************************************************************************
 * Filename: GameSpace.hpp
 * Author: Jens Bodal
 * Date: April 02, 2015
 * Description: describes the functionality of a GameSpace object for the
 * GameOfLife program
 *****************************************************************************/

#ifndef GAMESPACE_HPP
#define GAMESPACE_HPP

#include <string>

class GameSpace {
    private:
        std::string mValue;
        void setValue(std::string value) { mValue = value; }
    public:
        /***********************************************************************
         * Description: constructs a GameSpace object and sets its default value
         * to EMPTY
         * Parameters: none
         **********************************************************************/
        GameSpace();
        
        /***********************************************************************
         * Description: switches the value of the GameSpace object from EMPTY to
         * OCCUPIED or OCCUPIED to EMPTY
         * Parameters: none
         **********************************************************************/
        void switchValue();
        
        /***********************************************************************
         * Description: returns true if the GameSpace is currently empty
         * Parameters: none
         **********************************************************************/
        bool isEmpty();

        std::string getValue() { return mValue; }
};

#endif
