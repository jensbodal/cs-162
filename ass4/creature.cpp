/******************************************************************************
 * Filename: creature.cpp
 * Author: Jens Bodal
 * Date: May 07, 2015
 * Description: default implementation of certain functions and virtual methods
 * for the Creature class
 *****************************************************************************/

#include "creature.hpp"
#include <cstdlib>
#include <ctime>
#include <iostream>
#include <sstream>

Creature::Creature() {

}

Creature::Creature(Properties init) {
    std::srand(std::time(NULL));
    properties.name = init.name;
    properties.attack = init.attack;
    properties.defense = init.defense;
    properties.armor = init.armor;
    properties.strength = init.strength;
    setMaxStrength(init.strength);
    setDefenseRollModifier(1.00);
}

void Creature::Properties::setAttack(int numDice, int diceSides) {
    attack.numDice = numDice;
    attack.diceSides = diceSides;
}

void Creature::Properties::setDefense(int numDice, int diceSides) {
    defense.numDice = numDice;
    defense.diceSides = diceSides;
}

std::string Creature::toString() {
    std::stringstream output; 
    output << "[Name: " << getName() << "] ";
    output << "[Armor: " << getArmor() << "] ";
    output << "[Strength: " << getStrength() << "/" << getMaxStrength() << "] ";
    output << "[Owner: " << this->owner << "]";
    return output.str();
}

bool Creature::attack(Creature *creature) {
    int totalAttack = this->getAttackRoll();
    int totalDefense = creature->getDefenseRoll();
    return applyDamage(creature, totalAttack, totalDefense);
}

bool Creature::applyDamage(Creature *creature, int attRoll, int defRoll) {
    if (creature->isAlive()) {
        int totalDamage = attRoll - defRoll - creature->getArmor();
        totalDamage = totalDamage < 0 ? 0 : totalDamage;
        creature->setStrength(creature->getStrength() - totalDamage);
        return true;
    }
    else {
        return false;
    }
}

int Creature::rollDice(int numDice, int diceSides) {
    int rollSum = 0;
    for (int i = 0; i < numDice; i++) {
        int roll = std::rand() % diceSides + 1;
        rollSum += roll;
    }
    return rollSum;
}

int Creature::getAttackRoll() {
    return rollDice(getAttackNumDice(), getAttackDiceSides());
}

int Creature::getDefenseRoll() {
    int roll = rollDice(getDefenseNumDice(), getDefenseDiceSides());
    roll = (double)roll * getDefenseRollModifier();
    return roll;
}

int Creature::getDefenseRoll(Creature *creature) {
    return creature->getDefenseRoll();
}

void Creature::setStrength(int strength) {
    strength = strength < 0 ? 0 : strength;
    this->properties.strength = strength;
}

void Creature::setStrength(Creature* creature, int strength) {
    creature->setStrength(strength);
}

void Creature::setDefenseRollModifier(Creature *creature, double mod) {
    creature->setDefenseRollModifier(mod);
}

