/******************************************************************************
 * Filename: gameTest.cpp
 * Author: Jens Bodal
 * Date: May 07, 2015
 * Description: tests the implementation of the various Creatures in a
 * FantasyGame program
 * Input: none
 * Output: output of various test scenarios
 *****************************************************************************/

#include "creature.hpp"
#include "goblin.hpp"
#include "barbarian.hpp"
#include "reptilePeople.hpp"
#include "blueMen.hpp"
#include "theShadow.hpp"
#include "jqueue.hpp"
#include "jstack.hpp"
#include "player.hpp"

#include <iostream>

Creature selectFighter(std::string owner) {
    int selection;
    Creature creature;
    std::cout << "  1. Goblin\n";
    std::cout << "  2. Barbarian\n";
    std::cout << "  3. Reptile Person\n";
    std::cout << "  4. Blue Man\n";
    std::cout << "  5. The Shadow\n";
    std::cout << "    Selection: ";
    std::cin >> selection;
    switch(selection) {
        case 1:
            creature = Goblin();
            break;
        case 2:
            creature = Barbarian();
            break;
        case 3:
            creature = ReptilePeople();
            break;
        case 4:
            creature = BlueMen();
            break;
        case 5:
            creature = TheShadow();
            break;
        default:
            creature = Goblin();
            break;
    }
    creature.owner = owner;
    return creature;
}

int main() {

    int numFighters;
    // making the game 
    Player player1("Player 1");
    Player player2("Player 2");
    jens::jstack<Creature> losers;

    std::cout << "Welcome to tournament\n";
    std::cout << "How many fighters would you like each team to have?\n";
    std::cout << "  # fighters: ";
    std::cin >> numFighters;
    
    for (int i = 0; i < numFighters; i++) {
        std::cout << player1.name << ", select a fighter\n";
        player1.lineup.add(selectFighter(player1.name));

    }

    for (int i = 0; i < numFighters; i++) {
        std::cout << player2.name << " , select a fighter\n";
        player2.lineup.add(selectFighter(player2.name));
    }

    Creature *creature1 = &player1.lineup.peek();
    Creature *creature2 = &player2.lineup.peek();
    bool stop = false;
    int roundNum = 0;
    while (!player1.lineup.isEmpty() && !player2.lineup.isEmpty()) {
        std::cout << "\nRound " << ++roundNum << '\n';
        creature1->attack(creature2);
        std::cout << creature2->toString() << '\n';
        if (!creature2->isAlive()) {
            losers.add(player2.lineup.remove());
            if (!player2.lineup.isEmpty()) {
                creature2 = &player2.lineup.peek();
            }
            else {
                stop = true;
            }
        }
        else {
            creature2->attack(creature1);
            std::cout << creature1->toString() << '\n';
            if (!creature1->isAlive()) {
                losers.add(player1.lineup.remove());
                if (!player1.lineup.isEmpty()) {
                    creature1 = &player1.lineup.peek();
                }

                else {
                    stop = true;
                }
            }
        }
    } // end battle
   
    std::cout << "Calculating the loser.... .... ... \n";

    int numLoser = losers.size();
    int p1 = 0;
    int p2 = 0;
    for (int i = 0; i < numLoser; i++) {
        Creature *c = &losers.remove();
        if (c->owner == player1.name) {
            p1++;
        }
        else {
            p2++;
        }
    }
    if (p1 < p2) {
        std::cout << "Player 1 WON\n";
    }
    else if (p2 < p1) {
        std::cout << "PLayer 2 WON\n";
    }
    else {
        std::cout << "Game was a draw\n";
    }
    

}
