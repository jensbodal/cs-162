/******************************************************************************
 * Filename: barbarian.hpp
 * Author: Jens Bodal
 * Date: May 08, 2015
 * Description: definition of a barbarian creature
 *****************************************************************************/

#ifndef BARBARIAN_HPP
#define BARBARIAN_HPP

#include "creature.hpp"

class Barbarian : public Creature {
    public:
        Barbarian() : Creature(Defaults()) { }
        struct Defaults : public Creature::Properties {
            Defaults();
        };
};

#endif
