/******************************************************************************
 * Filename: blueMen.hpp
 * Author: Jens Bodal
 * Date: May 10, 2015
 * Description: definition of a Blue Men creature
 *****************************************************************************/

#ifndef BLUE_MEN_HPP
#define BLUE_MEN_HPP

#include "creature.hpp"

class BlueMen : public Creature {
    public:
        BlueMen() : Creature(Defaults()) { }
        struct Defaults : public Creature::Properties {
            Defaults();
        };
};
#endif
