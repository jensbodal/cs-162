/******************************************************************************
 * Filename: reptilePeople.hpp
 * Author: Jens Bodal
 * Date: May 10, 2015
 * Description: definition of a Reptile Men creature
 *****************************************************************************/

#ifndef REPTILE_PEOPLE_HPP
#define REPTILE_PEOPLE_HPP

#include "creature.hpp"

class ReptilePeople : public Creature {
    public:
        ReptilePeople() : Creature(Defaults()) { }
            struct Defaults : public Creature::Properties {
                Defaults();
            };
};

#endif
