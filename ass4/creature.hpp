/******************************************************************************
 * Filename: creature.hpp
 * Author: Jens Bodal
 * Date: May 07, 2015
 * Description: definition of a base abstract creature class for a fantasy game
 *****************************************************************************/

#ifndef CREATURE_HPP
#define CREATURE_HPP

#include <string>


class Creature {
    public:
        Creature();
        std::string getName() { return properties.name; }
        int getArmor() { return properties.armor; }
        int getStrength() { return properties.strength; }
        virtual std::string toString();
        virtual bool attack(Creature *creature);
        /***********************************************************************
         * Description: returns true if creature has more than or at least 1
         * strength
         * Parameters: none
         **********************************************************************/
        bool isAlive() { return this->getStrength() > 0; }
        void heal() {
            int health = getStrength();
            health = health < maxStrength ? (maxStrength - health) / 2 : 0;
            setStrength(getStrength() + health);
        }
        std::string owner;
    protected:
        /***********************************************************************
         * Description: Skill data type used to hold Attack or Defense skill
         * information
         * Parameters: none 
         **********************************************************************/
        struct Skill {
            int numDice;
            int diceSides;
        };

        /***********************************************************************
         * Description: Properties data type used to hold all attributes for a
         * Creature
         * Parameters: none
         **********************************************************************/
        struct Properties {
            std::string name;
            Skill attack;
            Skill defense;
            int armor;
            int strength;
            void setAttack(int numDice, int diceSides);
            void setDefense(int numDice, int diceSides);
        };

        /***********************************************************************
         * Description: Applies the damage to the creature 
         * Parameters: the creature to apply the damage to, the incoming attack
         * roll, the outgoing defense roll
         **********************************************************************/
        bool applyDamage(Creature *creature, int attackRoll, int defenseRoll);
        Creature(Properties);
        void setArmor(int armor) { properties.armor = armor; }

        /***********************************************************************
         * Description: sets the creatures strength, does not allow it to go
         * below 0
         * Parameters: the new strength to set the strength to
         **********************************************************************/
        virtual void setStrength(int strength);

        /***********************************************************************
         * Description: sets the strength of another creature 
         * Parameters: the creature to set the strength of, the value to set the
         * strength to
         **********************************************************************/
        void setStrength(Creature* creature, int strength);
        int getAttackNumDice() { return properties.attack.numDice; }
        int getAttackDiceSides() { return properties.attack.diceSides; }
        int getDefenseNumDice() { return properties.defense.numDice; }
        int getDefenseDiceSides() { return properties.defense.diceSides; }
        
        /***********************************************************************
         * Description: rolls a set of dice with the specified number of sides
         * Parameters: the number of dice to roll, the number of sides per die
         **********************************************************************/
        int rollDice(int numDice, int diceSides);

        /***********************************************************************
         * Description: obtains the attack roll for the creature
         * Parameters: none
         **********************************************************************/
        virtual int getAttackRoll();

        /***********************************************************************
         * Description: obtains the defense roll for the creature
         * Parameters: none
         **********************************************************************/
        virtual int getDefenseRoll();
        int getDefenseRoll(Creature *creature);

        /***********************************************************************
         * Description: sets the defense roll modifier for this creature
         * Parameters: the modifier to set the defense roll modifier to
         **********************************************************************/
        void setDefenseRollModifier(double mod) { this->defRollMod = mod; }

        /***********************************************************************
         * Description: sets the defense roll modifier for the specified
         * creature
         * Parameters: the creature to set the modifier of, the modifier to set 
         * the defense roll modifier to
         **********************************************************************/
        void setDefenseRollModifier(Creature *creature, double mod);
        double getDefenseRollModifier() { return this->defRollMod; }
    private:
        Properties properties;
        int maxStrength;
        void setMaxStrength(int value) { this->maxStrength = value; }
        int getMaxStrength() { return this->maxStrength; }
        double defRollMod;
};

#endif
