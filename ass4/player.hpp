/******************************************************************************
 * Filename: player.hpp
 * Author: Jens Bodal
 * Date: May 24, 2015
 * Description: 
 * Input: 
 * Output: 
 *****************************************************************************/

#ifndef PLAYER_HPP
#define PLAYER_HPP

#include "creature.hpp"
#include "jqueue.hpp"
#include <string>

class Player {
    public:
        Player()  {}
        Player(std::string);
        std::string name;
        jens::jqueue<Creature> lineup;
        int points;
};

Player::Player(std::string name) {
    this->name = name;
    this->points = 0;
}
#endif
