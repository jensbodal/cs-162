/******************************************************************************
 * Filename: lab1-2.cpp
 * Author: Jens Bodal
 * Date: April 02, 2015
 * Description: This contains the f test method which says that it has been
 * called
 *****************************************************************************/

#include "f.h"

#include <iostream>

void f() {
    std::cout << "This was function f!\n";
}
