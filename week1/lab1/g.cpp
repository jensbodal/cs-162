/******************************************************************************
 * Filename: lab1-2.cpp
 * Author: Jens Bodal
 * Date: April 02, 2015
 * Description: This contains the g test method which says that it has been
 * called
 *****************************************************************************/

#include "g.h"

#include <iostream>

void g() {
    std::cout << "This was function g!\n";
}
