/******************************************************************************
 * Filename: lab1-1.cpp
 * Author: Jens Bodal
 * Date: April 02, 2015
 * Description: this program will randomly generate three numbers, print them
 * out, then sort them from smallest to shortest
 * Input: none
 * Output: the original order of the numbers then the same numbers sorted
 *****************************************************************************/

#include <cstdlib>
#include <ctime>
#include <iostream>

const int MIN = -1000000;
const int MAX = 1000000;

/******************************************************************************
 * Description: obtains a random integer per constant min max values
 * Parameters: none
 *****************************************************************************/
int getNumber();

/******************************************************************************
 * Description: takes three integers, and rearranges them from the 1st
 * parameter to the 3rd from smallest to largest
 * Parameters: three pointers to integers
 *****************************************************************************/
void smallSort(int *num1, int *num2, int *num3);

/******************************************************************************
 * Description: takes two integer variables and swaps their values
 * Parameters: two pointers to integers
 *****************************************************************************/
void swap(int *num1, int *num2);

int main() {
    int seed = std::time(NULL);
    std::srand(seed);

    int num1 = getNumber();
    int num2 = getNumber();
    int num3 = getNumber();

    std::cout << "Number 1: " << num1 << std::endl;
    std::cout << "Number 2: " << num2 << std::endl;
    std::cout << "Number 3: " << num3 << std::endl;
    
    smallSort(&num1, &num2, &num3);

    std::cout << "\nAnd now they are sorted: \n";
    std::cout << "Number 1: " << num1 << std::endl;
    std::cout << "Number 2: " << num2 << std::endl;
    std::cout << "Number 3: " << num3 << std::endl;

    return 0;
}

void smallSort(int *num1, int *num2, int *num3) {
    if (*num1 > *num2) {
        swap(num1, num2);
    }

    if (*num2 > *num3) {
        swap(num2, num3);
    }

    if (*num1 > *num2) {
        swap(num1, num2);
    }
}

void swap(int *num1, int *num2) {
    int temp = *num1;
    *num1 = *num2;
    *num2 = temp;
}

int getNumber() {
    int range = MAX - MIN + 1;
    return std::rand() % range + MIN;
}
