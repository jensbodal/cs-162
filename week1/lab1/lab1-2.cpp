/******************************************************************************
 * Filename: lab1-2.cpp
 * Author: Jens Bodal
 * Date: April 02, 2015
 * Description: this program simply tests the functionality of using multiple
 * files, including headers, and implementing functions from them and calling
 * them
 * Input: none
 * Output: output from two test functions
 *****************************************************************************/

#include "f.h"
#include "g.h"

#include <iostream>

int main() {
    f();
    g();
    return 0;
}
