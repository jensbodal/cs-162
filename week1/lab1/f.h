/******************************************************************************
 * Filename: f.h
 * Author: Jens Bodal
 * Date: April 02, 2015
 * Description: header file for a test function prototype
 *****************************************************************************/

#ifndef F_H
#define F_H

/******************************************************************************
 * Description: test function which shows that it has been called
 * Parameters: none
 *****************************************************************************/
void f();

#endif
