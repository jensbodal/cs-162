/******************************************************************************
 * Filename: g.h
 * Author: Jens Bodal
 * Date: April 02, 2015
 * Description: header file for a test function prototype
 *****************************************************************************/

#ifndef G_H
#define G_H

/******************************************************************************
 * Description: test function which shows that it has been called
 * Parameters: none
 *****************************************************************************/
void g();

#endif
