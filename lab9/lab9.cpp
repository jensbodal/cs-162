/******************************************************************************
 * Filename: lab9.cpp
 * Author: Jens Bodal
 * Date: May 29, 2015
 * Description: this is a test driver for the QueueBuffer and StackPal classes
 *****************************************************************************/

#include "stackPal.hpp"
#include "queueBuffer.hpp"
#include <iostream>

int main() {
    StackPal palindrome;
    QueueBuffer myBuffer;
    double percent;
    bool doingStuff = true;

    for (int i = 0; i < 10; i++) {
        int j = i > 1 ? 5 : 1;
        while (j > 0) {
            std::cout << "Palindrome [characters: " << i << "]: ";
            std::cout << palindrome.getPalindrome(i) << '\n';
            --j;
        }
    }

    std::cout << "\n\n\nWe will now simulate a buffer\n";

    int round = 0;
    int sizes = 0;
    while (doingStuff) {   
        std::cout << "Please enter a percentage in decimal form (e.g. 20% = 0.20)";
        std::cout << " for the chance that  value will be created and put in the";
        std::cout << " queue\n";
        std::cout << "Percentage: ";
        std::cin >> percent;

        if (myBuffer.addValue(percent)) {
            std::cout << "[" << myBuffer.getLastAdded() << "]";
            std::cout << " has been added to the queue!\n";
        }

        else {
            std::cout << "No value was created and nothing was added to queue\n";
        }

        std::cout << "Percentage that a value will be removed: ";
        std::cin >> percent;

        if (myBuffer.removeValue(percent)) {
            std::cout << "[" << myBuffer.getLastRemoved() << "]";
            std::cout << " has been removed from the queue\n";
        }
        else {
            std::cout << "No value was removed from the queue\n";
        }

        sizes += myBuffer.getSize();
        int average = sizes / (++round);

        std::cout << std::endl;
        std::cout << "[Round: " << round << "] [Current size: ";
        std::cout << myBuffer.getSize() << "] [Average size: " << average<<"]";
        std::cout << std::endl;
    
    }
    
    return 0;
}
