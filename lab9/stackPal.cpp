/******************************************************************************
 * Filename: stackPal.cpp
 * Author: Jens Bodal
 * Date: May 29, 2015
 * Description: this class is used to generate a palindrome of random characters
 * using the number of characters as the basis for the length of the palindrome
 * length = (number of characters) * 2 - 1
 *****************************************************************************/

#include "stackPal.hpp"

#include <cstdlib>
#include <ctime>
#include <stack>
#include <string>

StackPal::StackPal() {
    std::srand(time(NULL));
}

std::string StackPal::getPalindrome(int length) {
    std::string myPalindrome = "";
	
    if (length == 0) { return ""; }

    while (length > 0) {
	myPalindrome += getRandomChar();

	std::stack<char> tempStack;
	if (myPalindrome.length() > 1) {
	    for (int i = 0; i < myPalindrome.length(); i++) {
		tempStack.push(myPalindrome[i]);
	    }
	    std::string tempString = "";
	    tempString += tempStack.top();
	    myPalindrome.insert(0, tempString);
	}
	--length;
    }
        
    return myPalindrome;
    
}

char StackPal::getRandomChar() {
    int min = 97;
    int max = 122;
    return std::rand() % (max - min + 1) + min;
}
