/******************************************************************************
 * Filename: queueBuffer.cpp
 * Author: Jens Bodal
 * Date: May 29, 2015
 * Description: QueueBuffer class simulates a buffer by using a queue.  User
 * specificies chance that a value will be added or removed from the queue and
 * then the output can be seen
 *****************************************************************************/

#include "queueBuffer.hpp"
#include <ctime>
#include <cstdlib>

QueueBuffer::QueueBuffer() { 
    std::srand(time(NULL));
}

bool QueueBuffer::addValue(double chance) {
    chance *= 100;
    // we don't need the precision so just using int value
    int chanceInt = chance;
    chanceInt = chanceInt > 100 ? 100 : chanceInt;
    chanceInt = chanceInt < 0 ? 0 : chanceInt;
    
    int min = 0;
    int max = 100;
    int compareValue = std::rand() % (max - min + 1) + min;
   
    if (compareValue > chanceInt) {
        return false;
    }

    else {
        myQueue.push(compareValue);
        return true;
    }
}

bool QueueBuffer::removeValue(double chance) {
    chance *= 100;
    // we don't need the precision so just using int value
    int chanceInt = chance;
    chanceInt = chanceInt > 100 ? 100 : chanceInt;
    chanceInt = chanceInt < 0 ? 0 : chanceInt;
    
    int min = 0;
    int max = 100;
    int compareValue = std::rand() % (max - min + 1) + min;
   
    if (compareValue > chanceInt) {
        return false;
    }

    else if (myQueue.size() > 0) {
        lastRemoved = myQueue.front();
        myQueue.pop();
        return true;
    }

    else {
        return false;
    }
}

int QueueBuffer::getLastAdded() {
    return myQueue.back();
}

int QueueBuffer::getLastRemoved() {
    return this->lastRemoved;
}

int QueueBuffer::getSize() {
    return myQueue.size();
}
