/******************************************************************************
 * Filename: queueBuffer.hpp
 * Author: Jens Bodal
 * Date: May 29, 2015
 * Description: definition file for the queueBuffer class
 *****************************************************************************/

#ifndef QUEUE_BUFFER_HPP
#define QUEUE_BUFFER_HPP

#include <queue>

class QueueBuffer {
    private:
	std::queue<int> myQueue;
	int lastRemoved;
	
    public:

        /***********************************************************************
         * Description: Creates a QueueBuffer and seeds the random number
	 * generator
         * Parameters: none
         **********************************************************************/
	QueueBuffer();

        /***********************************************************************
         * Description: adds a random value to the queue from 1 - 100 based on
	 * the provided percent chance
         * Parameters: the chance that the value will be added to the buffer
         **********************************************************************/
	bool addValue(double chance);

        /***********************************************************************
         * Description: removes a random value from the queue form 1 - 100 based
	 * on the provided percent chance
         * Parameters: the chance that the value will be removed from the buffer
         **********************************************************************/
	bool removeValue(double chance);

        /***********************************************************************
         * Description: obtains the last value added to the buffer
         * Parameters: none
         **********************************************************************/
	int getLastAdded();

        /***********************************************************************
         * Description: obtains the last value removed from the buffer
         * Parameters: none
         **********************************************************************/
	int getLastRemoved();

        /***********************************************************************
         * Description: returns the number of values that are currently in the
	 * buffer
         * Parameters: none
         **********************************************************************/
	int getSize();

};

#endif
