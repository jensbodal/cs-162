/******************************************************************************
 * Filename: stackPal.hpp
 * Author: Jens Bodal
 * Date: May 29, 2015
 * Description: defintion of the stackPal class 
 *****************************************************************************/

#ifndef STACK_PAL_HPP
#define STACK_PAL_HPP

#include <string>

class StackPal {
    private:
        char getRandomChar();
    public:
        StackPal();

        /***********************************************************************
         * Description: creates a palindrome using the number of characters
         * specified; e.g. 2 would create a palindrome of length 3, 3 would be
         * length 5, 4 would be length 7
         * length = characters * 2 - 1
         * Parameters: number of characters to use
         **********************************************************************/
        std::string getPalindrome(int characters);
};

#endif
