/******************************************************************************
 * Filename: startRoom.cpp
 * Author: Jens Bodal
 * Date: June 08, 2015
 * Description: 
 * Input: 
 * Output: 
 *****************************************************************************/

#include "roomController.hpp"
#include "startRoom.hpp"
#include "player.hpp"


StartRoom::StartRoom(std::string name, int number, RoomController *controller)
    : IRoom(name, number, controller) {
    
    setupOptions();
}

void StartRoom::linkRooms() {
    room2 = getController()->getRoom2();
    room3 = getController()->getRoom3();
    room6 = getController()->getRoom6();
    room7 = getController()->getRoom7();
}

std::string StartRoom::getLevelText() {
    return "You are in the Starting Room!";
}

void StartRoom::setupOptions() {
    options.push_back("1. Go North Room 2");
    options.push_back("2. Go East to Room 3");
    options.push_back("3. Go West to Room 6");
    options.push_back("4. Go South to Room 7");
}

void StartRoom::receiveOption(int option, Player *player) {
    IRoom *newRoom;
    switch(option) {
        case 1:
            newRoom = room2;
            break;
        case 2:
            newRoom = room3;
            break;
        case 3:
            newRoom = room6;
            break;
        case 4:
            newRoom = room7;
            break;
        default:
            newRoom = this;
            break;
        
    }
    player->setRoom(newRoom);
}

IRoom * StartRoom::getRoom(int choice) {
    switch(choice) {
        case 1:
            return this->room2;
        case 2:
            return room3;
        case 3:
            return room6;
        case 4:
            return room7;
        default:
            return this;
    }
}
