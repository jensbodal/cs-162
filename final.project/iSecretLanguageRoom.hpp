/******************************************************************************
 * Filename: iSecretLanguageRoom.hpp
 * Author: Jens Bodal
 * Date: June 08, 2015
 * Description: 
 * Input: 
 * Output: 
 *****************************************************************************/

#ifndef I_SECRET_LANGUAGE_ROOM_HPP
#define I_SECRET_LANGUAGE_ROOM_HPP

#include "iSpecialRoom.hpp"

class ISecretLanguageRoom : public ISpecialRoom {
    private:
        int encryptionKey;
        bool encrypted;
    protected:
        void setupSpecialOptions();
    public:
        ISecretLanguageRoom(std::string name, int number, RoomController *);
        void receiveSpecialOption(int, Player *);
        std::string getSuccessMessage();
        std::string getFailMessage();
        void takeControl();
        void encrypt(std::string &str);
        void decrypt(std::string &str);

};

#endif


