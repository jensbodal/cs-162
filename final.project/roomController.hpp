/******************************************************************************
 * Filename: roomController.hpp
 * Author: Jens Bodal
 * Date: June 08, 2015
 * Description: 
 * Input: 
 * Output: 
 *****************************************************************************/

#ifndef ROOM_CONTROLLER_HPP
#define ROOM_CONTROLLER_HPP

#include "iRoom.hpp"
#include "startRoom.hpp"
#include "room2.hpp"
#include "room3.hpp"
#include "room4.hpp"
#include "room5.hpp"
#include "room6.hpp"
#include "room7.hpp"
#include "room8.hpp"
#include "room9.hpp"
#include "finalRoom.hpp"


class RoomController {
    private:
        StartRoom *startRoom;
        Room2 *room2;
        Room3 *room3;
        Room4 *room4;
        Room5 *room5;
        Room6 *room6;
        Room7 *room7;
        Room8 *room8;
        Room9 *room9;
        FinalRoom *finalRoom;
        
    public:
        RoomController();
        ~RoomController();
        IRoom *getStartRoom() { return this->startRoom; }
        IRoom *getRoom2() { return this->room2; }
        IRoom *getRoom3() { return this->room3; }
        IRoom *getRoom4() { return this->room4; }
        IRoom *getRoom5() { return this->room5; }
        IRoom *getRoom6() { return this->room6; }
        IRoom *getRoom7() { return this->room7; }
        IRoom *getRoom8() { return this->room8; }
        IRoom *getRoom9() { return this->room9; }
        IRoom *getFinalRoom() { return this->finalRoom; }
};

#endif
