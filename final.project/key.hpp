/******************************************************************************
 * Filename: key.hpp
 * Author: Jens Bodal
 * Date: June 08, 2015
 * Description: 
 * Input: 
 * Output: 
 *****************************************************************************/

#ifndef KEY_HPP
#define KEY_HPP

class Key;

typedef std::vector<Key*> KeyRing;

class Key {
    private:
        int code;
    public:
        Key() { this->code = -1; }
        Key(int code) {
            this->code = code;
        }
        bool equals(Key *key) {
            return (this->getCode() == key->getCode());
        }
        int getCode() {
            return this->code;
        }
        void setCode(int code) { this->code = code; }
};

#endif
