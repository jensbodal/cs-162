/******************************************************************************
 * Filename: iSpecialRoom.hpp
 * Author: Jens Bodal
 * Date: June 09, 2015
 * Description: 
 * Input: 
 * Output: 
 *****************************************************************************/

#ifndef I_SPECIAL_ROOM_HPP
#define I_SPECIAL_ROOM_HPP

#include "iRoom.hpp"

class ISpecialRoom : public IRoom {
    private:
        bool resetFlag;
        bool haveControl;
    protected:
        Options specialOptions;
        virtual void setupSpecialOptions() = 0;
        void setControl(bool control) { haveControl = control; }
    public:
        ISpecialRoom(std::string name, int number, RoomController *controller)
            : IRoom(name, number, controller) {
            setResetFlag(false);            
            setRestrictions(true);
            haveControl = false;
        }
        Options * getSpecialOptions() { return &(this->specialOptions); }
        virtual void receiveSpecialOption(int option, Player *player) = 0;
        virtual std::string getSuccessMessage() = 0; 
        virtual std::string getFailMessage() = 0;
        bool getResetFlag() { return this->resetFlag; }
        void setResetFlag(bool flag) { this->resetFlag = flag; }
        bool wantsControl() { return this->haveControl; }
        virtual void takeControl() { }

};

#endif
