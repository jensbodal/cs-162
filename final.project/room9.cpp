/******************************************************************************
 * Filename: room9.cpp
 * Author: Jens Bodal
 * Date: June 08, 2015
 * Description: 
 * Input: 
 * Output: 
 *****************************************************************************/

#include "roomController.hpp"
#include "room9.hpp"
#include "player.hpp"

Room9::Room9(std::string name, int number, RoomController *controller)
    : IRoom(name, number, controller) {

    setupOptions();
}

void Room9::linkRooms() {
    room4 = getController()->getRoom4();
    room5 = getController()->getRoom5();
    room8 = getController()->getRoom8();
    finalRoom = getController()->getFinalRoom();

}

std::string Room9::getLevelText() {
    return "You are in Room 9!";
}

void Room9::setupOptions() {
    options.push_back("1. Go North to Room 10");
    options.push_back("2. Go South to Room 5");
    options.push_back("3. Go East to Room 4");
    options.push_back("4. Go West to Room 8");
}

void Room9::receiveOption(int option, Player *player) {
    IRoom *newRoom;
    switch(option) {
        case 1:
            newRoom = finalRoom;
            break;
        case 2:
            newRoom = room5;
            break;
        case 3:
            newRoom = room4;
            break;
        case 4:
            newRoom = room8;
            break;
        default:
            newRoom = this;
            break;

    }
    player->setRoom(newRoom);

}
