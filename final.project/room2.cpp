/******************************************************************************
 * Filename: room2.cpp
 * Author: Jens Bodal
 * Date: June 08, 2015
 * Description: 
 * Input: 
 * Output: 
 *****************************************************************************/

#include "roomController.hpp"
#include "room2.hpp"
#include "player.hpp"

Room2::Room2(std::string name, int number, RoomController *controller)
    : IBombRoom(name, number, controller) {

    setupOptions();
}

void Room2::linkRooms() {
    startRoom = getController()->getStartRoom();
    room3 = getController()->getRoom3();
    room4 = getController()->getRoom4();
    room6 = getController()->getRoom6();
}

std::string Room2::getLevelText() {
    return "You are in Room 2!"; 
}

void Room2::setupOptions() {
    options.push_back("1. Go West to Room 6");
    options.push_back("2. Go South to the Start Room");
    options.push_back("3. Go South to Room 3");
    options.push_back("4. Go South to Room 4");
}

void Room2::receiveOption(int option, Player *player) {
    switch(option) {
        case 1:
            player->setRoom(room6);
            break;
        case 2:
            player->setRoom(startRoom);
            break;
        case 3:
            player->setRoom(room3);
            break;
        case 4:
            player->setRoom(room4);
        default:
            break;
    }
}

