/******************************************************************************
 * Filename: room2.hpp
 * Author: Jens Bodal
 * Date: June 08, 2015
 * Description: 
 * Input: 
 * Output: 
 *****************************************************************************/

#ifndef ROOM_2_HPP
#define ROOM_2_HPP

#include "iBombRoom.hpp"

class Room2 : public IBombRoom {
    private:
        IRoom *startRoom;
        IRoom *room3;
        IRoom *room4;
        IRoom *room6;
    protected:
        void setupOptions();
    public:
        Room2(std::string name, int number, RoomController *);
        std::string getLevelText();
        void receiveOption(int option, Player *player);
        IRoom *getRoom(int choice) { return this; }
        void linkRooms();
};

#endif
