/******************************************************************************
    std::cout << "Going back back";
 * Filename: roomController.cpp
 * Author: Jens Bodal
 * Date: June 08, 2015
 * Description: 
 * Input: 
 * Output: 
 *****************************************************************************/

#include "roomController.hpp"

RoomController::RoomController() {
    startRoom = new StartRoom("Starting Room", 1, this); 
    room2 = new Room2("Room 2", 2, this);
    room3 = new Room3("Room 3", 3, this);
    room4 = new Room4("Room 4", 4, this);
    room5 = new Room5("Room 5", 5, this);
    room6 = new Room6("Room 6", 6, this);
    room7 = new Room7("Room 7", 7, this);
    room8 = new Room8("Room 8", 8, this);
    room9 = new Room9("Room 9", 9, this);
    finalRoom = new FinalRoom("Final Room", 10, this);
    startRoom->linkRooms();
    room2->linkRooms();
    room3->linkRooms();
    room4->linkRooms();
    room5->linkRooms();
    room6->linkRooms();
    room7->linkRooms();
    room8->linkRooms();
    room9->linkRooms();
    finalRoom->linkRooms();

}

RoomController::~RoomController() {
    delete startRoom;
    delete room2;
    delete room3;
    delete room4;
    delete room5;
    delete room6;
    delete room7;
    delete room8;
    delete room9;
    delete finalRoom;
}
