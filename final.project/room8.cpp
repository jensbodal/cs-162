/******************************************************************************
 * Filename: room8.cpp
 * Author: Jens Bodal
 * Date: June 08, 2015
 * Description: 
 * Input: 
 * Output: 
 *****************************************************************************/

#include "roomController.hpp"
#include "room8.hpp"
#include "player.hpp"

Room8::Room8(std::string name, int number, RoomController *controller)
    : IRoom(name, number, controller) {

    setupOptions();
}

void Room8::linkRooms() {
    room5 = getController()->getRoom5();
    room6 = getController()->getRoom6();
    room7 = getController()->getRoom7();
    room9 = getController()->getRoom9();

}


std::string Room8::getLevelText() {
    return "You are in Room 8!";
}

void Room8::setupOptions() {
    options.push_back("1. Go North to Room 7");
    options.push_back("2. Go East to Room 9");
    options.push_back("3. Go South to ROom 5");
    options.push_back("4. Go West to Room 6");
}

void Room8::receiveOption(int option, Player *player) {
    IRoom *newRoom;
    switch(option) {
        case 1:
            newRoom = room7;
            break;
        case 2:
            newRoom = room9;
            break;
        case 3:
            newRoom = room5;
            break;
        case 4:
            newRoom = room6;
            break;
        default:
            newRoom = this;
            break;

    }
    player->setRoom(newRoom);

}
