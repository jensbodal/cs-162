/******************************************************************************
 * Filename: iRoom.hpp
 * Author: Jens Bodal
 * Date: June 08, 2015
 * Description: 
 * Input: 
 * Output: 
 *****************************************************************************/

#ifndef	I_ROOM_HPP
#define I_ROOM_HPP

class Player;
class RoomController;

#include "door.hpp"

#include <string>
#include <vector>

typedef std::vector<std::string> Options;

class IRoom {

    private:
        std::string name;
        RoomController *controller;
        bool restrictions;
        int number;
        void setName(std::string name) { this->name = name; }
        void setNumber(int number) { this->number = number; }

    protected:

        /***********************************************************************
         * Description: contains the options for the room which drive the menu
         * Parameters: 
         **********************************************************************/
        Options options;

        /***********************************************************************
         * Description: identifies the room as being special or not
         * Parameters: bool value
         **********************************************************************/
        void setRestrictions(bool value) { restrictions = value; }

        /***********************************************************************
         * Description: sets up the options -- call from constructor of child
         * classes
         * Parameters: none
         **********************************************************************/
        virtual void setupOptions() = 0; 
    public:

        /***********************************************************************
         * Description: links up the rooms to the other associated rooms
         * Parameters: none 
         **********************************************************************/
        virtual void linkRooms() = 0;

        /***********************************************************************
         * Description: creates a new room and associates with ac tonrollrer
         * Parameters: the name of the room, id number, and controller
         **********************************************************************/
        IRoom(std::string name, int number, RoomController *);
        virtual ~IRoom() { }
        std::string getName() { return this->name; }
        int getNumber() { return this->number; }

        /***********************************************************************
         * Description: basic level text for room
         * Parameters: 
         **********************************************************************/
        virtual std::string getLevelText() = 0;

        /***********************************************************************
         * Description: returns the options for the room
         * Parameters: none
         **********************************************************************/
        Options * getOptions() { return &(this->options); }
        virtual void receiveOption(int option, Player *player) = 0;
        RoomController *getController() { return this->controller; }
        virtual IRoom *getRoom(int choice) = 0;

        /***********************************************************************
         * Description: returns true if this is a special room and has
         * restrictions
         * Parameters: none
         **********************************************************************/
        bool hasRestrictions() { return this->restrictions; }
};

#endif
