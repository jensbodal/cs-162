/******************************************************************************
 * Filename: room7.cpp
 * Author: Jens Bodal
 * Date: June 08, 2015
 * Description: 
 * Input: 
 * Output: 
 *****************************************************************************/

#include "roomController.hpp"
#include "room7.hpp"
#include "player.hpp"

Room7::Room7(std::string name, int number, RoomController *controller)
    : IBombRoom(name, number, controller) {

    setupOptions();
}

void Room7::linkRooms() {
    startRoom = getController()->getStartRoom();
    room6 = getController()->getRoom6();
    room8 = getController()->getRoom8();
    finalRoom = getController()->getFinalRoom();

}

std::string Room7::getLevelText() {
    return "You are in Room 7!";
}

void Room7::setupOptions() {
    options.push_back("1. Go North to Start Room");
    options.push_back("2. Go East to Room 10");
    options.push_back("3. Go South to ROom 8");
    options.push_back("4. Go West to Room 6");
}

void Room7::receiveOption(int option, Player *player) {
    IRoom *newRoom;
    switch(option) {
        case 1:
            newRoom = startRoom;
            break;
        case 2:
            newRoom = finalRoom;
            break;
        case 3:
            newRoom = room8;
            break;
        case 4:
            newRoom = room6;
            break;
        default:
            newRoom = this;
            break;

    }
    player->setRoom(newRoom);

}
