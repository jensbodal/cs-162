/******************************************************************************
 * Filename: mainProgram.cpp
 * Author: Jens Bodal
 * Date: June 08, 2015
 * Description: This is the main driver for a text-based game where a player
 * navigates different rooms, solving puzzles along the way in order to rescue
 * the hostages in the final room.
 *****************************************************************************/

#include "iRoom.hpp" // for RoomOptions
#include "iSpecialRoom.hpp"
#include "roomController.hpp"
#include "player.hpp"
#include <ctime>
#include <limits>
#include <iostream>
#include <string>

int getInteger();
std::string getString();

int main() {
    bool running = true;
    RoomController roomController;
    std::string playerName;
    int selection;
    std::time_t start,end;
    std::time (&start);
    double elapsed_seconds = 0;
    int timeLimit = 120;

    std::cout << "Welcome to Hostage Rescue\n";
    std::cout << "Please enter your name: ";
    std::cin >> playerName;
    Player player(playerName, &roomController);
    std::cout << "Welcome " << player.getName() << '\n';
    std::cout << "You must rescue the hostages!  Navigate through the";
    std::cout << " rooms and find the key to rescue the hostages.\n";
    std::cout << std::endl;
    
    IRoom *room;
    Options *options;
    ISpecialRoom *sRoom;
    Options *sOptions;

    while(running) {
        if (elapsed_seconds > timeLimit) {
            std::cout << "You ran out of time!!!!\n";
            running = false;
            break;
        }
        room = player.getCurrentRoom();
        bool lvlTextPrinted = true;
        options = room->getOptions();
        bool restricted = room->hasRestrictions();
        while(restricted) {
            std::cout << room->getLevelText() << '\n';
            sRoom = static_cast<ISpecialRoom*> (room);
            sOptions = sRoom->getSpecialOptions();
            for (int i = 0; i < sOptions->size(); i++) {
                std::cout << sOptions->at(i) << '\n';
            }

            if (sRoom->wantsControl()) {
                sRoom->takeControl();
                selection = getInteger();
                player.sendSpecialOption(selection);
            }
            else {
                std::cout << "Selection: ";
                selection = getInteger();
                player.sendSpecialOption(selection);
            }
            if (!sRoom->hasRestrictions()) {
                std::cout << "\nSuccess!\n";
                std::cout << sRoom->getSuccessMessage() << "\n\n";
                lvlTextPrinted = false;
            }
            
            else {
                std::cout << sRoom->getFailMessage() << "\n\n";
            }
            
            if (sRoom->getResetFlag()) {
                sRoom->setResetFlag(false);
                player.goBack();
                restricted = false;

            }
            
            else {
                restricted = sRoom->hasRestrictions();
            }

        }
        
        if (!player.isComplete()) {
            room = player.getCurrentRoom();
            std::cout << room->getLevelText() << '\n';
            options = room->getOptions();
            std::cout << player.getInventory();

            for (int i = 0; i < options->size(); i++) {
                std::cout << options->at(i) << '\n';
            }
            std::cout << "Selection: ";
            selection = getInteger();
            player.sendOption(selection);
            std::cout << "\n\n";
        }
        std::time (&end);
        elapsed_seconds = std::difftime(end, start);
        std::cout << std::endl;
        std::cout << "################################################\n";
        std::cout << "Elapsed time (seconds): " << elapsed_seconds << '\n';
        std::cout << "################################################\n";
        running = !(player.isComplete());

    }
    return 0;
}

int getInteger() {
    bool getting = true;
    int returnInt;
    while (getting) {
        std::cin >> returnInt;
        if (std::cin.fail() || std::cin.peek() != '\n') {
            std::cin.clear();
            std::cin.ignore(std::numeric_limits<int>::max(), '\n');
            std::cout << "\nYou did not enter an integer! Try again: ";
        }
        else {
            getting = false;
        }
    }
    return returnInt;
}

std::string getString() {
    bool getting = true;
    std::string returnStr;
    while (getting) {
        std::cin >> returnStr;
        getting = false;
    }
    return returnStr;
}
