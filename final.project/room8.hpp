/******************************************************************************
 * Filename: room8.hpp
 * Author: Jens Bodal
 * Date: June 08, 2015
 * Description: 
 * Input: 
 * Output: 
 *****************************************************************************/

#ifndef ROOM_8_HPP
#define ROOM_8_HPP

#include "iRoom.hpp"

class Room8 : public IRoom {
    private:
        // linked to 6, 7, 9, 5
        IRoom *room5;
        IRoom *room6;
        IRoom *room7;
        IRoom *room9;

    protected:
        void setupOptions();

    public:
        Room8(std::string name, int number, RoomController *controller);
        std::string getLevelText();
        void receiveOption(int option, Player *player);
        IRoom *getRoom(int choice) { return this; }
        void linkRooms();

};


#endif
