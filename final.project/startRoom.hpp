/******************************************************************************
 * Filename: startRoom.hpp
 * Author: Jens Bodal
 * Date: June 08, 2015
 * Description: 
 * Input: 
 * Output: 
 *****************************************************************************/

#ifndef START_ROOM_HPP
#define START_ROOM_HPP

#include "iRoom.hpp"

class StartRoom : public IRoom {
    private:
        IRoom *room2;
        IRoom *room3;
        IRoom *room7;
        IRoom *room6;

    protected:
        void setupOptions();
    
    public:
        StartRoom(std::string name, int number, RoomController *);
        std::string getLevelText();
        void receiveOption(int option, Player *player);
        IRoom * getRoom(int choice);
        void linkRooms();
};

#endif
