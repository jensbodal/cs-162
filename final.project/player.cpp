/******************************************************************************
 * Filename: player.cpp
 * Author: Jens Bodal
 * Date: June 08, 2015
 * Description: 
 * Input: 
 * Output: 
 *****************************************************************************/

#include "player.hpp"
#include "iSpecialRoom.hpp"
#include <sstream>

Player::Player(std::string name, RoomController *rooms) {
	setName(name);
        this->rooms = rooms;
        currentRoom = rooms->getStartRoom();
        previousRoom = rooms->getStartRoom();
        complete = false;
}

std::string Player::getCurrentRoomName() {
    return getCurrentRoom()->getName();
}

void Player::sendOption(int option) {
    getCurrentRoom()->receiveOption(option, this);
}

void Player::sendSpecialOption(int option) {
    ISpecialRoom *sRoom = static_cast<ISpecialRoom*>(currentRoom);
    sRoom->receiveSpecialOption(option, this);
}

void Player::setRoom(IRoom *room) {
    previousRoom = getCurrentRoom();
    currentRoom = room;
}

void Player::goBack() {
    setRoom(previousRoom);
}

void Player::pickupKey(int code) {
    keys.push_back(new Key(code)); 
}

std::string Player::getInventory() {
    std::stringstream ss;
    ss << "### INVENTORY ###\n";
    for (int i = 0; i < keys.size(); i++) {
        ss << "Key: ";
        ss << keys.at(i)->getCode();
        ss << '\n';
    }
    ss << "### END INVENTORY ###\n";
    return ss.str();
}
