/******************************************************************************
 * Filename: iBombRoom.cpp
 * Author: Jens Bodal
 * Date: June 08, 2015
 * Description: 
 * Input: 
 * Output: 
 *****************************************************************************/

#include "iBombRoom.hpp"
#include <sstream>

IBombRoom::IBombRoom(std::string name, int number, RoomController *controller)
            : ISpecialRoom(name, number, controller) {

    setupSpecialOptions();
    diffuseColor = "red";
}

void IBombRoom::setupSpecialOptions() {
    specialOptions.push_back("There is a bomb in this room! Cut the correct wire in order to continue!");
    specialOptions.push_back("1. Cut the red wire");
    specialOptions.push_back("2. Cut the blue wire");
    specialOptions.push_back("3. Cut the green wire");
}

void IBombRoom::receiveSpecialOption(int option, Player *player) {
    std::string color;
    switch(option) {
        case 1:
            color = "red";
            break;
        case 2:
            color = "blue";
            break;
        case 3:
            color = "green";
            break;
        default:
            break;
    }
    if (color == diffuseColor) {
        setRestrictions(false);
    }
}

std::string IBombRoom::getSuccessMessage() {
    std::string retString = "";
    std::stringstream ss;
    ss << "You have diffused the bomb!\n";
    ss << "This room is now safe!";

    retString = ss.str();
    return retString;
}

std::string IBombRoom::getFailMessage() {
    std::string retString = "";
    std::stringstream ss;

    ss << "You chose the wrong color :((((((((\n";
    ss << "The bomb has exploded and you must now go back to the beginning";
    
    setResetFlag(true);
    retString = ss.str();
    return retString;
}

