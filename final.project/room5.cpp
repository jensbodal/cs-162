/******************************************************************************
 * Filename: room5.cpp
 * Author: Jens Bodal
 * Date: June 08, 2015
 * Description: 
 * Input: 
 * Output: 
 *****************************************************************************/

#include "roomController.hpp"
#include "room5.hpp"
#include "player.hpp"

Room5::Room5(std::string name, int number, RoomController *controller)
    : ISecretLanguageRoom(name, number, controller) {

    setupOptions();
}

void Room5::linkRooms() {
    room4 = getController()->getRoom4();
    room6 = getController()->getRoom6();
    room8 = getController()->getRoom8();
    room9 = getController()->getRoom9();

}

std::string Room5::getLevelText() {
    return "You are in Room 5!";
}

void Room5::setupOptions() {
    options.push_back("1. Go East to Room 4");
    options.push_back("2. Go North to Room 9");
    options.push_back("3. Go North to Room 8");
    options.push_back("4. Go North to Room 6");
}

void Room5::receiveOption(int option, Player *player) {
    IRoom *newRoom;
    switch(option) {
        case 1:
            newRoom = room4;
            break;
        case 2:
            newRoom = room9;
            break;
        case 3:
            newRoom = room8;
            break;
        case 4:
            newRoom = room6;
            break;
        default:
            newRoom = this;
            break;

    }
    player->setRoom(newRoom);

}
