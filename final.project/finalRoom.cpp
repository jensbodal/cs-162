/******************************************************************************
 * Filename: finalRoom.cpp
 * Author: Jens Bodal
 * Date: June 08, 2015
 * Description: 
 * Input: 
 * Output: 
 *****************************************************************************/

#include "roomController.hpp"
#include "finalRoom.hpp"
#include "player.hpp"
#include "key.hpp"

#include <sstream>

FinalRoom::FinalRoom(std::string name, int number, RoomController *controller)
    : ISpecialRoom(name, number, controller) {

    setupOptions();
    setupSpecialOptions();
    finalDoorKey.setCode(1337);
    setRestrictions(true);
}

void FinalRoom::linkRooms() {
    room3 = getController()->getRoom3();
    room4 = getController()->getRoom4();
    room7 = getController()->getRoom7();
    room9 = getController()->getRoom9();
    
}

std::string FinalRoom::getLevelText() {
    return "This room is locked";
}

void FinalRoom::setupOptions() {
    options.push_back("1. Go North to Room 3");
    options.push_back("2. Go East to Room 4");
    options.push_back("3. Go South to Room 9");
    options.push_back("4. Go West to Room 7");
}

void FinalRoom::receiveOption(int option, Player *player) {
    IRoom *newRoom;
    switch(option) {
        case 1:
            newRoom = room3;
            break;
        case 2:
            newRoom = room4;
            break;
        case 3:
            newRoom = room9;
            break;
        case 4:
            newRoom = room7;
            break;
        default:
            newRoom = this;
            break;

    }
    player->setRoom(newRoom);

}

void FinalRoom::setupSpecialOptions() {
    Options *so = &specialOptions;
    so->push_back("Try key?");
    so->push_back("1. Yes");
    so->push_back("2. No");
}

void FinalRoom::receiveSpecialOption(int option, Player *player) {
    KeyRing *keys = player->getKeyRing();
    switch(option) {
        case 1:
            for (int i = 0; i < keys->size(); i++) {
                if (keys->at(i)->equals(&(this->finalDoorKey))) {
                    setRestrictions(false);
                    player->setComplete(true);
                }
            }
            break;
    }
}

std::string FinalRoom::getSuccessMessage() {
    std::string retString;
    std::stringstream ss;

    ss << "Congratulations, you have the correct key!";
    ss << "\nYou have saved the hostages!";

    retString = ss.str();
    return retString;

}

std::string FinalRoom::getFailMessage() {
    std::string retString;
    std::stringstream ss;
    
    ss << "You must have a key to enter this room!";
    setResetFlag(true);
    retString = ss.str();
    return retString;
}



