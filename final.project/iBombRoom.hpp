/******************************************************************************
 * Filename: iBombRoom.hpp
 * Author: Jens Bodal
 * Date: June 08, 2015
 * Description: 
 * Input: 
 * Output: 
 *****************************************************************************/

#ifndef I_BOMB_ROOM_HPP
#define I_BOMB_ROOM_HPP

#include "iSpecialRoom.hpp"

class IBombRoom : public ISpecialRoom {
    private:
        std::string diffuseColor;
    protected:
        void setupSpecialOptions();
    public:
        IBombRoom(std::string name, int number, RoomController *controller);
        void receiveSpecialOption(int, Player*);
        std::string getSuccessMessage();
        std::string getFailMessage();

};

#endif
