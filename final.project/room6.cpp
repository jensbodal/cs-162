/******************************************************************************
 * Filename: room6.cpp
 * Author: Jens Bodal
 * Date: June 08, 2015
 * Description: 
 * Input: 
 * Output: 
 *****************************************************************************/

#include "roomController.hpp"
#include "room6.hpp"
#include "player.hpp"

Room6::Room6(std::string name, int number, RoomController *controller)
    : IBombRoom(name, number, controller) {

    setupOptions();
}

void Room6::linkRooms() {
    startRoom = getController()->getStartRoom();
    room2 = getController()->getRoom2();
    room5 = getController()->getRoom5();
    room7 = getController()->getRoom7();
    room8 = getController()->getRoom8();
}

std::string Room6::getLevelText() {
    return "You are in Room 6!";
}

void Room6::setupOptions() {
    options.push_back("1. Go South to Room 5");
    options.push_back("2. Go East to Room 8");
    options.push_back("3. Go East to Room 7");
    options.push_back("4. Go East to Room 1");
    options.push_back("5. Go east to Room 2");
}

void Room6::receiveOption(int option, Player *player) {
    IRoom *newRoom;
    switch(option) {
        case 1:
            newRoom = room5;
            break;
        case 2:
            newRoom = room8;
            break;
        case 3:
            newRoom = room7;
            break;
        case 4:
            newRoom = startRoom;
            break;
        case 5:
            newRoom = room2;
            break;
        default:
            newRoom = this;
            break;

    }
    player->setRoom(newRoom);

}
