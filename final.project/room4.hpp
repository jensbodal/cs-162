/******************************************************************************
 * Filename: room4.hpp
 * Author: Jens Bodal
 * Date: June 08, 2015
 * Description: 
 * Input: 
 * Output: 
 *****************************************************************************/

#ifndef ROOM_4_HPP
#define ROOM_4_HPP

#include "iMathRoom.hpp"

class Room4 : public IMathRoom {
    private:
        // linked to 2, 3, 10, 9, 5
        IRoom *room2;
        IRoom *room3;
        IRoom *room5;
        IRoom *room9;
        IRoom *finalRoom;

    protected:
        void setupOptions();

    public:
        Room4(std::string name, int number, RoomController *);
        std::string getLevelText();
        void receiveOption(int option, Player *player);
        IRoom *getRoom(int choice) { return this; }
        void linkRooms();

};

#endif
