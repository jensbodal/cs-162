/******************************************************************************
 * Filename: room9.hpp
 * Author: Jens Bodal
 * Date: June 08, 2015
 * Description: 
 * Input: 
 * Output: 
 *****************************************************************************/

#ifndef ROOM_9_HPP
#define ROOM_9_HPP

#include "iRoom.hpp"

class Room9 : public IRoom {
    private:
        // linked to 4, 5, 8, final
        IRoom *room4;
        IRoom *room5;
        IRoom *room8;
        IRoom *finalRoom;

    protected:
        void setupOptions();

    public:
        Room9(std::string name, int number, RoomController *);
        std::string getLevelText();
        void receiveOption(int option, Player *player);
        IRoom *getRoom(int choice) { return this; }
        void linkRooms();

};

#endif
