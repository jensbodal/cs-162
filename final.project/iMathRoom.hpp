/******************************************************************************
 * Filename: iMathRoom.hpp
 * Author: Jens Bodal
 * Date: June 08, 2015
 * Description: 
 * Input: 
 * Output: 
 *****************************************************************************/

#ifndef I_MATH_ROOM_HPP
#define I_MATH_ROOM_HPP

#include "iSpecialRoom.hpp"

class IMathRoom : public ISpecialRoom {
    private:
        int answer;
    protected:
        void setupSpecialOptions();

    public:
        IMathRoom(std::string name, int number, RoomController *controller);
        void receiveSpecialOption(int, Player *);
        std::string getSuccessMessage();
        std::string getFailMessage();


};

#endif


