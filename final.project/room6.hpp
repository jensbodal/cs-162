/******************************************************************************
 * Filename: room6.hpp
 * Author: Jens Bodal
 * Date: June 08, 2015
 * Description: 
 * Input: 
 * Output: 
 *****************************************************************************/

#ifndef ROOM_6_HPP
#define ROOM_6_HPP

#include "iBombRoom.hpp"

class Room6: public IBombRoom {
    private:
        // linked to rooms 1, 2, 5, 7, 8
        IRoom *startRoom;
        IRoom *room2;
        IRoom *room5;
        IRoom *room7;
        IRoom *room8;

    protected:
        void setupOptions();

    public:
        Room6(std::string name, int number, RoomController *);
        std::string getLevelText();
        void receiveOption(int option, Player *player);
        IRoom *getRoom(int choice) { return this; }
        void linkRooms();
        
};

#endif
