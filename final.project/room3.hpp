/******************************************************************************
 * Filename: room3.hpp
 * Author: Jens Bodal
 * Date: June 08, 2015
 * Description: 
 * Input: 
 * Output: 
 *****************************************************************************/

#ifndef ROOM_3_HPP
#define ROOM_3_HPP

#include "iBombRoom.hpp"

class Room3 : public IBombRoom {
    private:
        IRoom *startRoom;
        IRoom *room2;
        IRoom *room4;
        IRoom *finalRoom;
    protected:
        void setupOptions();
    public:
        Room3(std::string name, int number, RoomController *);
        std::string getLevelText();
        void receiveOption(int option, Player *player);
        IRoom *getRoom(int choice) { return this; }
        void linkRooms();
};

#endif
