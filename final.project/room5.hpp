/******************************************************************************
 * Filename: room5.hpp
 * Author: Jens Bodal
 * Date: June 08, 2015
 * Description: 
 * Input: 
 * Output: 
 *****************************************************************************/

#ifndef ROOM_5_HPP
#define ROOM_5_HPP

#include "iSecretLanguageRoom.hpp"

class Room5 : public ISecretLanguageRoom {
    private:
        // linked to rooms 4, 6, 8, 9
        IRoom *room4;
        IRoom *room6;
        IRoom *room8;
        IRoom *room9;

    protected:
        void setupOptions();

    public:
        Room5(std::string name, int number, RoomController *);
        std::string getLevelText();
        void receiveOption(int option, Player *player);
        IRoom *getRoom(int choice) { return this; }
        void linkRooms();

};

#endif
