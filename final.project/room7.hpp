/******************************************************************************
 * Filename: room7.hpp
 * Author: Jens Bodal
 * Date: June 08, 2015
 * Description: 
 * Input: 
 * Output: 
 *****************************************************************************/

#ifndef ROOM_7_HPP
#define ROOM_7_HPP

#include "iBombRoom.hpp"

class Room7 : public IBombRoom {
    private:
        // linked to 6, 8, start, final
        IRoom *startRoom;
        IRoom *room6;
        IRoom *room8; 
        IRoom *finalRoom;

    protected:
        void setupOptions();

    public:
        Room7(std::string name, int number, RoomController *);
        std::string getLevelText();
        void receiveOption(int option, Player *player);
        IRoom *getRoom(int choice) { return this; }
        void linkRooms();
        
};

#endif
