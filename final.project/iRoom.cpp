/******************************************************************************
 * Filename: iRoom.cpp
 * Author: Jens Bodal
 * Date: June 08, 2015
 * Description: 
 * Input: 
 * Output: 
 *****************************************************************************/

#include "iRoom.hpp"

IRoom::IRoom(std::string name, int number, RoomController *controller) {
	setName(name);
	setNumber(number);
        this->controller = controller;
        setRestrictions(false);
}

