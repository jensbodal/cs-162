/******************************************************************************
 * Filename: iMathRoom.cpp
 * Author: Jens Bodal
 * Date: June 08, 2015
 * Description: 
 * Input: 
 * Output: 
 *****************************************************************************/

#include "iMathRoom.hpp"
#include <sstream>

IMathRoom::IMathRoom(std::string name, int number, RoomController *controller)
            : ISpecialRoom(name, number, controller) { 

    answer = 13*15*21*44*33;
    setupSpecialOptions();            
    setControl(false);
            
}

void IMathRoom::setupSpecialOptions() {
    Options *so = &specialOptions;
    so->push_back("A wizard of math has demanded you prove your worth!");
    so->push_back("Choose which combination of numbers, when multiplied");
    so->push_back(" together equal the special answer\n");
    std::stringstream ss;
    ss << "The special answer is: [" << answer << "]\n";
    so->push_back(ss.str());
    so->push_back("1. [2, 328, 22, 1]");
    so->push_back("2. [13, 15, 21, 44, 33]");
    so->push_back("3. [33, 23, 1, 19, 33]");
    so->push_back("4. [10, 23, 11, 49, 3]");

}

void IMathRoom::receiveSpecialOption(int option, Player *player) {
    if (option == 2) {
        setRestrictions(false);
    }
}

std::string IMathRoom::getSuccessMessage() {
    std::string retString = "";
    std::stringstream ss;
    ss << "You might be a wizard yourself!\n";
    ss << "Your answer was correct, you may proceed";
    retString = ss.str();
    return retString;
}

std::string IMathRoom::getFailMessage() {
    std::string retString = "";
    std::stringstream ss;
    
    ss << "Your answer was incorrect, please try again.";
    setResetFlag(true);

    retString = ss.str();
    return retString;
}



