/******************************************************************************
 * Filename: finalRoom.hpp
 * Author: Jens Bodal
 * Date: June 08, 2015
 * Description: 
 * Input: 
 * Output: 
 *****************************************************************************/

#ifndef FINAL_ROOM_HPP
#define FINAL_ROOM_HPP

#include "iSpecialRoom.hpp"
#include "key.hpp"

class FinalRoom : public ISpecialRoom {
    private:
        // linked to 3, 4, 7, 9
        IRoom *room3;
        IRoom *room4;
        IRoom *room7;
        IRoom *room9;
        Key finalDoorKey;

    protected:
        void setupOptions();
        void setupSpecialOptions();

    public:
        FinalRoom(std::string name, int number, RoomController *controller);
        std::string getLevelText();
        void receiveOption(int option, Player *player);
        IRoom *getRoom(int choice) { return this; }
        void linkRooms();
        void receiveSpecialOption(int option, Player *player);
        std::string getSuccessMessage();
        std::string getFailMessage();


};

#endif
