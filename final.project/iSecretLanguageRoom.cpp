/******************************************************************************
 * Filename: iSecretLanguageRoom.cpp
 * Author: Jens Bodal
 * Date: June 08, 2015
 * Description: 
 * Input: 
 * Output: 
 *****************************************************************************/

#include "iSecretLanguageRoom.hpp"
#include "player.hpp"
#include <iostream>


ISecretLanguageRoom::ISecretLanguageRoom(
        std::string name, int number, RoomController *controller)
    : ISpecialRoom(name, number, controller) { 
   
    encryptionKey = 1;
    encrypted = true;
    setupSpecialOptions();
    setControl(true);
}

void ISecretLanguageRoom::setupSpecialOptions() {
    Options *so = &specialOptions;
    so->push_back("Something isn't right...");
}

void ISecretLanguageRoom::receiveSpecialOption(int option, Player *player) {
    switch(option) {
        case 1:
            encrypted = false;
            break;
        case 1337:
            setRestrictions(false);
            player->pickupKey(1337);
            break;
        default:
            setResetFlag(true);
            break;
    }
}

std::string ISecretLanguageRoom::getSuccessMessage() {
    std::string retString = "You found the key!";
    return retString;
}

std::string ISecretLanguageRoom::getFailMessage() {
    std::string retString = "";
    retString = "You did not succeed, you must go back to the previous room";
    return retString;
}

void ISecretLanguageRoom::takeControl() {
    std::string message;
    message = "Enter 1337 to pick up the key";
    encrypt(message);
    if (!encrypted) {
        decrypt(message);
    }
    std::cout << message << '\n';
    if (encrypted) {
        std::cout << "The one you want is the second word: ";
    }
    else {
        std::cout << "Enter password: ";
    }

}

void ISecretLanguageRoom::encrypt(std::string &str) {
    for (int i = 0; i < str.length(); i++) {
        str[i] += encryptionKey;
    }
}

void ISecretLanguageRoom::decrypt(std::string &str) {
    for (int i = 0; i < str.length(); i++) {
        str[i] -= encryptionKey;
    }
}
