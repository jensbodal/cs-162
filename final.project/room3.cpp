/******************************************************************************
 * Filename: room3.cpp
 * Author: Jens Bodal
 * Date: June 08, 2015
 * Description: 
 * Input: 
 * Output: 
 *****************************************************************************/

#include "roomController.hpp"
#include "room3.hpp"
#include "player.hpp"

Room3::Room3(std::string name, int number, RoomController *controller)
    : IBombRoom(name, number, controller) {
    
    setupOptions();
}

void Room3::linkRooms() {
    startRoom = getController()->getStartRoom();
    room2 = getController()->getRoom2();
    room4 = getController()->getRoom4();
    finalRoom = getController()->getFinalRoom();

}

std::string Room3::getLevelText() {
    return "You are in Room 3!";
}

void Room3::setupOptions() {
    options.push_back("1. Go West to Start Room");
    options.push_back("2. Go North to Room 2");
    options.push_back("3. Go East to Room 4");
    options.push_back("4. Go South to the Room 10");
}

void Room3::receiveOption(int option, Player *player) {
    IRoom *newRoom;
    switch(option) {
        case 1:
            newRoom = startRoom;
            break;
        case 2:
            newRoom = room2;
            break;
        case 3:
            newRoom = room4;
            break;
        case 4:
            newRoom = finalRoom;
            break;
        default:
            newRoom = this;
            break;

    }
    player->setRoom(newRoom);
}
