/******************************************************************************
 * Filename: room4.cpp
 * Author: Jens Bodal
 * Date: June 08, 2015
 * Description: 
 * Input: 
 * Output: 
 *****************************************************************************/

#include "roomController.hpp"
#include "room4.hpp"
#include "player.hpp"

Room4::Room4(std::string name, int number, RoomController *controller)
    : IMathRoom(name, number, controller) {
    
    setupOptions();
}

void Room4::linkRooms() {
    room2 = getController()->getRoom2();
    room3 = getController()->getRoom3();
    room5 = getController()->getRoom5();
    room9 = getController()->getRoom9();
    finalRoom = getController()->getFinalRoom();

}

std::string Room4::getLevelText() {
    return "You are in Room 4!";
}

void Room4::setupOptions() {
    options.push_back("1. Go North to Room 2");
    options.push_back("2. Go East to Room 3");
    options.push_back("3. Go East to Room 10");
    options.push_back("4. Go East to Room 9");
    options.push_back("5. Go East to Room 5");
}

void Room4::receiveOption(int option, Player *player) {
    IRoom *newRoom;
    switch(option) {
        case 1:
            newRoom = room2;
            break;
        case 2:
            newRoom = room3;
            break;
        case 3:
            newRoom = finalRoom;
            break;
        case 4:
            newRoom = room9;
            break;
        case 5:
            newRoom = room5;
            break;
        default:
            newRoom = this;
            break;

    }
    player->setRoom(newRoom);

}
