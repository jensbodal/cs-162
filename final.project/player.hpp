/******************************************************************************
 * Filename: player.hpp
 * Author: Jens Bodal
 * Date: June 08, 2015
 * Description: 
 * Input: 
 * Output: 
 *****************************************************************************/

#ifndef PLAYER_HPP
#define PLAYER_HPP

#include "roomController.hpp"
#include "iRoom.hpp"
#include "key.hpp"

#include <string>
#include <vector>

class Player {
    private:
        IRoom *currentRoom;
        IRoom *previousRoom;
        RoomController *rooms;
        std::string name;
        KeyRing keys;
        void setName(std::string name) { this-> name = name; }
        bool complete;

    public:
        Player(std::string name, RoomController*);

        /***********************************************************************
         * Description: sets the room of the player
         * Parameters: the room to send the player to
         **********************************************************************/
        void setRoom(IRoom *room);

        /***********************************************************************
         * Description: gets the current room of the player
         * Parameters: none
         **********************************************************************/
        IRoom * getCurrentRoom() { return this->currentRoom; }

        /***********************************************************************
         * Description: returns a pointer to the controller that the player is
         * using which contains the rooms
         * Parameters: none
         **********************************************************************/
        RoomController * getRooms() { return this->rooms; }

        /***********************************************************************
         * Description: obtains the string value of the room, useful for
         * debugging
         * Parameters: none
         **********************************************************************/
        std::string getCurrentRoomName();

        /***********************************************************************
         * Description: returns the player's name
         * Parameters: none
         **********************************************************************/
        std::string getName() { return this->name; }

        /***********************************************************************
         * Description: sends an option to a basic room, useful for the menu
         * options
         * Parameters: the integer option for the room's function
         **********************************************************************/
        void sendOption(int option);

        /***********************************************************************
         * Description: sends a special option to a special room
         * Parameters: the option to send
         **********************************************************************/
        void sendSpecialOption(int option);

        /***********************************************************************
         * Description: sends the player back to the previous room
         * Parameters: none
         **********************************************************************/
        void goBack();

        /***********************************************************************
         * Description: picks up a key with the specified key
         * Parameters: the code of the key to pickup
         **********************************************************************/
        void pickupKey(int code);

        /***********************************************************************
         * Description: returns a string representing the player's inventory
         * Parameters: none
         **********************************************************************/
        std::string getInventory();

        /***********************************************************************
         * Description: tells the game if the player has completed his tasks
         * Parameters: boolean value of status
         **********************************************************************/
        void setComplete(bool status) { this->complete = status; }

        /***********************************************************************
         * Description:  returns true if the player has compelted all tasks
         * Parameters: none
         **********************************************************************/
        bool isComplete() { return this->complete; }

        /***********************************************************************
         * Description: obtains they KeyRing for the player
         * Parameters: none
         **********************************************************************/
        KeyRing * getKeyRing() { return &(this->keys); }

};

#endif
