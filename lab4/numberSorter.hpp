/******************************************************************************
 * Filename: numberSorter.hpp
 * Author: Jens Bodal
 * Date: April 26, 2015
 * Description: defines a number sorter object
 *****************************************************************************/

#ifndef NUMBER_SORTER_HPP
#define NUMBER_SORTER_HPP

#include <fstream>
#include <string>

class NumberSorter {
    private:
        typedef std::ifstream inputFile;
        typedef std::ofstream outputFile;
    public:
        /***********************************************************************
         * Description: creates a NumberSorter object 
         * Parameters: none
         **********************************************************************/
        NumberSorter();

        /***********************************************************************
         * Description: takes two ifstream text files and an ofstream file and
         * sorts integers that are in the first two files and outputs them in
         * ascending order to the outputfile
         * Parameters: two ifstream objects and the ofstream object to write to
         **********************************************************************/
        void sort(inputFile* iFile1, inputFile* iFile2, outputFile* oFile);
};

#endif
