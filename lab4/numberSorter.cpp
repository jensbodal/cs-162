/******************************************************************************
 * Filename: numberSorter.cpp
 * Author: Jens Bodal
 * Date: April 26, 2015
 * Description: this program will take two files which contain integers then
 * output all of the integers in ascending order to a specified output file
 * Input: the name of the files to read and write to/from
 * Output: output written to specified output file
 *****************************************************************************/

#include "numberSorter.hpp"
#include <fstream>
#include <iostream>

int main() {
    NumberSorter sorter;
    std::string fileName1;
    std::string fileName2;
    std::string ofName;

    std::cout << "Enter the name of the first input file: ";
    std::cin >> fileName1;
    std::cout << "Enter the name of the second input file: ";
    std::cin >> fileName2;
    std::cout << "Enter the name of the output file (note contents will be";
    std::cout << " deleted): ";
    std::cin >> ofName;

    std::ifstream file1(fileName1.c_str());
    std::ifstream file2(fileName2.c_str());
    std::ofstream oFile(ofName.c_str());
    sorter.sort(&file1, &file2, &oFile);
    
    return 0;
}

NumberSorter::NumberSorter() {
    // empty constructor
}

void NumberSorter::sort(inputFile* iFile1, inputFile* iFile2, outputFile* oFile) {
    int number1;
    int number2;
    bool getNext1 = true; // tracks if we need another number from file1
    bool getNext2 = true; // tracks if we need another number from file2
    bool done1 = false; // tracks if file1 is at the end of file
    bool done2 = false; // tracks if file2 is at the end of file
    int i = 0;
    
    if (iFile1->good() && iFile2->good()) {
        while (!iFile1->eof() || !iFile2->eof()) {
            if (getNext1) {
                *iFile1 >> number1;
            }
            
            if (getNext2) {
                *iFile2 >> number2;
            }

            getNext1 = number1 <= number2;
            getNext2 = number2 <= number1;

            if (done1) {
                getNext1 = false;
                getNext2 = true;
            }

            if (done2) {
                getNext1 = true;
                getNext2 = false;
            }

            if (done1 && done2) {
                getNext1 = getNext2 = false;
            }

            if (getNext1) {
                *oFile << number1 << '\n';
            }

            if (getNext2) {
                *oFile << number2 << '\n';
            }

            done1 = iFile1->eof();
            done2 = iFile2->eof();
        }
        
        iFile1->close();
        iFile2->close();
        oFile->close();
    }
    else {
        std::cout << "One of your input filenames was invalid!\n";
    }
}

